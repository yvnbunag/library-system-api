<!-- omit in toc -->
# Library System API

A midterm project for the university course
`Software Development with Quality Assurance`

<br/>

---

<!-- omit in toc -->
## Contents
- [Requirements](#requirements)
- [Consumers](#consumers)
- [Containerized Development](#containerized-development)
- [Standalone Development](#standalone-development)
- [Production build](#production-build)
- [Credentials](#credentials)
- [API](#api)
  - [Endpoints](#endpoints)
  - [Usage](#usage)
- [Environment Variables](#environment-variables)

<br/>

---

## Requirements

1. [NodeJS](https://nodejs.org/en/) version 16.6 or higher
2. [MySQL](https://www.mysql.com/) version 5.6 or higher

<br/>

---

## Consumers

1. [Library System](https://gitlab.com/yvnbunag/library-system)

<br/>

---

## Containerized Development

See [Library System Stack](https://gitlab.com/yvnbunag/library-system-stack)
repository

<br/>

---

## Standalone Development

1. Install dependencies
    ```sh
    yarn install
    ```
2. Set up environment variables
    ```sh
    # Copy pre configured env file for local development
    cp .env.local .env
    ```

    **OR**

    ```sh
    # Copy base env file then populate
    cp .env.dist .env
    ```
3. Create database to be used by the service
    ```sh
    # `username`, `password` and `database` from environment variables `DB_USER`, `DB_PASSWORD` and `DB`
    mysql -u [username] -p -e "CREATE DATABASE [database]"
    ```
4. Sync database models
    ```sh
    yarn database:sync
    ```
5. Seed database with test entries
    ```sh
    yarn database:seed
    ```
    > The SQL file being used to seed is
    > [vars/library_system_2-5-19.sql](vars/library_system_2-5-19.sql)
6. Start the service on watch mode
    ```sh
    yarn dev
    ```

> Rest API is now available in [http://localhost:3010](http://localhost:3010) or
> the value of the `API_URL` environment variable

<br/>

---

## Production build

1. Install dependencies
    ```sh
    yarn install
    ```
2. Build the service
    ```sh
    yarn build
    ```
3. Remove development dependencies
    ```sh
    yarn install --production
    ```
4. Start the service
    ```sh
    yarn start
    ```

> When deploying, the `dist`, `bin` and `config` directories and the `app.js`
> entry-point should be part of the package

<br/>

---

## Credentials

The following are credentials included in the database seed, stored in the users
table

| id 	| username  	| password  	| type          	|
|----	|-----------	|-----------	|---------------	|
| 24 	| 201400000 	| 201400000 	| administrator 	|
| 25 	| 201400001 	| 201400001 	| librarian     	|
| 26 	| 201400002 	| 201400002 	| student       	|

Take note that the seed user credentials are only usable with the password
hash and encryption keys of the `.env.local` environment

When using a different set of keys for the current seed, passwords may be
updated through the `Internal/User management` APIs `UPDATE user` or
`IMPORT users`

<br/>

---

## API

API is not documented, but may be inspected with
[Postman](https://www.postman.com/) via the exported collection in
[vars/library_system.postman_collection.json](vars/library_system.postman_collection.json)

<br/>

### Endpoints

#### Application

Endpoints used within the
[Library System](https://gitlab.com/yvnbunag/library-system) application.
Composed of the following APIs:
- Users
- Borrows
- Resource management
- Borrow management

These APIs are authenticated with a JWT token issued from a valid login using
the `Internal/Security/AUTHENTICATE user` endpoint

Management APIs are only allowed to be used by `administrator` and `librarian`
users

<br/>

#### Internal

Endpoints used to manage and authorize users and operations. Composed of of the
following APIs:
- User management
- Security
- Worker management

These APIs are authenticated with an application token, currently implemented as
a mock JWT token and only verified for equality (used like a basic token)

The purpose of this token is to authorize library system applications when
consuming internal APIs

[Postman](https://www.postman.com/) is the current application used to interface
these APIs for consumption

<br/>

### Usage

1. Import [collection](vars/library_system.postman_collection.json) file to
    the [Postman application](https://www.postman.com/)
2. Create and use an environment for the Library System
3. Add a `library-system-api` variable
   - This is the base route of all the requests
   - Use the value of `API_URL` environment variable
   - All postman requests are injected an `Origin` header from this variable
4. To use **Internal APIs**, add a `library-system-api-mock-access-token`
    variable
   - Use the value of `API_ACCESS_REGISTERED_MOCK_TOKEN` environment variable
5. To use **Application APIs**, Add a `library-system-app-access-token` variable
   - You may generate a token using the `Internal/Security/AUTHENTICATE user`
        endpoint
   - Generated tokens last 24 hours, a new token must be generated upon expiry

> Some requests have usage guides / notes. Expand the request's description to
> view

> All postman requests inherit their authorization tokens from their API group

<br/>

---

## Environment Variables
1. PORT
   - Service port
2. API_URL
   - URL of the service
3. APPLICATION_URL
   - URL of the
     [Library System API](https://gitlab.com/yvnbunag/library-system-api)
4. JWT_PASSPHRASE
    - Secret key for JWT generation and validation
5. ENCRYPTION_KEY
    - Password credential encryption key
6. HASHING_KEY
    - Password credential hashing key
7. API_ACCESS_REGISTERED_MOCK_TOKEN
   - Token used to authorize other applications / services when communicating
     with the
     [Library System API](https://gitlab.com/yvnbunag/library-system-api)
   - Currently implemented as a mock bearer token
8.  DB
    - Database name
9.  DB_HOST
    - Database host
10. DB_USER
    - Database username
11. DB_PASSWORD
    - Database password
12. DB_DIALECT
    - Database dialect
    - Configuration for Sequelize ORM
    - Service uses **mysql**
13. MAILING_SERVICE
    - Flag to determine if mailing service is available and may be consumed
    - Used in email notification of:
      - Borrowed resource/s that are near their due date
      - Borrowed resources/s that are overdue
14. SENDGRID_API_KEY
    - API key of SendGrid, the mailing client
15. MAILING_SERVICE_DEFAULT_SENDER
    - Email address to use in the email notifications
