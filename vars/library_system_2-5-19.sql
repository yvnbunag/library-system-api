-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 05, 2019 at 09:09 AM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `library_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `borrow`
--

CREATE TABLE IF NOT EXISTS `borrow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `resource_id` int(11) NOT NULL,
  `status` varchar(10) NOT NULL,
  `date_approved` varchar(20) DEFAULT NULL,
  `date_due` varchar(20) DEFAULT NULL,
  `date_returned` varchar(20) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=192;

--
-- Dumping data for table `borrow`
--

REPLACE INTO `borrow` (`id`, `user_id`, `resource_id`, `status`, `date_approved`, `date_due`, `date_returned`, `created_at`, `updated_at`) VALUES
(127, 26, 90, 'rejected', NULL, NULL, NULL, '2019-01-01 09:37:31', '2019-01-01 09:37:59'),
(128, 26, 83, 'returned', '1546425748', '1546598548', '1546598548', '2019-01-01 09:37:35', '2019-01-01 09:38:12'),
(129, 26, 90, 'returned', '1546339348', '1546512148', '1546512148', '2019-01-01 09:38:15', '2019-01-01 09:40:13'),
(130, 26, 56, 'returned', '1546508315', '1546681115', '1546681115', '2019-01-01 09:58:28', '2019-01-01 09:29:31'),
(132, 26, 100, 'lent', '1546508407', '1546681207', NULL, '2019-01-03 09:39:58', '2019-01-03 09:40:07'),
(133, 26, 99, 'rejected', NULL, NULL, NULL, '2019-01-03 09:40:34', '2019-01-04 01:13:51'),
(134, 24, 99, 'returned', '1546339348', '1547203348', '1548392072', '2018-12-31 09:49:53', '2019-01-25 04:54:34'),
(136, 24, 98, 'returned', '1546563370', '1546736170', '1546736170', '2019-01-04 00:55:50', '2019-01-04 00:56:18'),
(137, 24, 97, 'returned', '1546564445', '1546737245', '1546737245', '2019-01-04 01:14:00', '2019-01-04 01:14:21'),
(140, 25, 91, 'returned', '1546914959', '1547087759', '1547088759', '2019-01-04 08:24:57', '2019-01-08 05:59:05'),
(142, 25, 96, 'returned', '1546927175', '1547099975', '1547194715', '2019-01-08 05:59:14', '2019-01-11 08:12:37'),
(147, 24, 97, 'returned', '1547016714', '1547189514', '1547016822', '2019-01-09 00:30:12', '2019-01-09 06:53:42'),
(148, 26, 97, 'rejected', NULL, NULL, NULL, '2019-01-09 00:50:34', '2019-01-09 00:51:40'),
(149, 24, 94, 'returned', '1547019093', '1547191893', '1547019105', '2019-01-09 07:06:49', '2019-01-09 07:31:45'),
(151, 24, 98, 'rejected', NULL, NULL, NULL, '2019-01-11 08:06:58', '2019-01-11 08:07:33'),
(171, 24, 84, 'lent', '1547195086', '1547367886', NULL, '2019-01-11 08:17:28', '2019-01-11 08:24:32'),
(181, 25, 84, 'lent', '1547195093', '1547367893', NULL, '2019-01-11 08:18:28', '2019-01-11 08:18:56'),
(191, 24, 99, 'pending', NULL, NULL, NULL, '2019-01-25 04:54:56', '2019-01-25 04:54:56');

-- --------------------------------------------------------

--
-- Table structure for table `resources`
--

CREATE TABLE IF NOT EXISTS `resources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `details` varchar(510) NOT NULL,
  `count` int(11) DEFAULT NULL,
  `borrowed` int(11) DEFAULT NULL,
  `classification` varchar(15) NOT NULL,
  `type` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=432;

--
-- Dumping data for table `resources`
--

REPLACE INTO `resources` (`id`, `title`, `details`, `count`, `borrowed`, `classification`, `type`) VALUES
(56, 'The Math Book', '{"author":"Clifford A. Pickover","publication_details":"Sterling Pub. Co. Inc., c.2009","call_number":"BM01 TMB 2009"}', 2, 0, 'document', 'book'),
(57, 'How Not to Be Wrong: The Power of Mathematical Thinking', '{"author":"Jordan Ellenberg","publication_details":"Penguin Group c.2014","call_number":"BM02 TMT 2014"}', 3, 0, 'document', 'book'),
(58, 'Math Curse', '{"author":"Jon Scieszka","publication_details":"Viking Press c.1995","call_number":"BM03 MC 1995"}', 3, 0, 'document', 'book'),
(59, 'What Is Mathematics', '{"author":"Richard Courant","publication_details":"Oxford University Press c.1941","call_number":"BM04 WIM 1941"}', 1, 0, 'document', 'book'),
(60, 'Essential Calculus: Early Transcendentals', '{"author":"James Stewart","publication_details":"Cengage Learning c.2007","call_number":"BM05 ECET 2007"}', 1, 0, 'document', 'book'),
(61, 'English Grammar in Use', '{"author":"Raymond Murphy","publication_details":"Cambridge University Press c.1985","call_number":"BE01 EGU 1985"}', 3, 0, 'document', 'book'),
(62, 'The Blue Book of Grammar and Punctuation', '{"author":"Jane Straus","publication_details":"Jossey-Bass 1999","call_number":"BE02 BBGP 1999"}', 4, 0, 'document', 'book'),
(63, 'Practical English Usage', '{"author":"Michael Swan","publication_details":"Oxford University Press c.1980","call_number":"BE03 PEU 1980"}', 1, 0, 'document', 'book'),
(64, 'The Cambridge Grammar of the English Language', '{"author":"Geoffrey K. Pullum","publication_details":"Cambridge University Press c.2002","call_number":"BE04 CGEL 2002"}', 5, 0, 'document', 'book'),
(65, 'Basic English Grammar', '{"author":"Betty Schrampfer Azar","publication_details":"Pearson Education c.2014","call_number":"BE05 BEG 2014"}', 3, 0, 'document', 'book'),
(66, 'University Physics', '{"author":"Hugh D. Young","publication_details":"Pearson Education c.1949","call_number":"BS01 UP 1949"}', 3, 0, 'document', 'book'),
(67, 'Seven Brief Lessons on Physics', '{"author":"Carlo Rovelli","publication_details":"Penguin Books c.2014","call_number":"BS02 SBLP 2014"}', 1, 0, 'document', 'book'),
(68, 'Concepts of Physics', '{"author":"H. C. Verma","publication_details":"Bharti bhawan c.1992","call_number":"BS03 CP 1992"}', 3, 0, 'document', 'book'),
(69, 'In Search of Schrödinger''s Cat: Quantum Physics and Reality', '{"author":"John Gribbin","publication_details":"Bantam Books c.1984","call_number":"BS04 ISSC 1984"}', 2, 0, 'document', 'book'),
(70, 'Hyperspace', '{"author":"Michio Kaku","publication_details":"Oxford University Press c.1994","call_number":"BS05 HYP 1994"}', 2, 0, 'document', 'book'),
(71, 'Head First Java', '{"author":"Kathy Sierra","publication_details":"O''Reilly Media, Inc. c.2003","call_number":"BP01 HFJ 2003"}', 1, 0, 'document', 'book'),
(72, 'Thinking in Java', '{"author":"Bruce Eckel","publication_details":"Prentice Hall c.2006","call_number":"BP02 TJ 2006"}', 2, 0, 'document', 'book'),
(73, 'Object-Oriented JavaScript', '{"author":"Stoyan Stefanov","publication_details":"Packt Publishing c.2008","call_number":"CP03 OOJ 2008"}', 5, 0, 'document', 'book'),
(74, 'Learning PHP and MySQL', '{"author":"Michele E. Davis","publication_details":"O''Reilly Media, Inc. c.2006","call_number":"CP04 LPM 2006"}', 2, 0, 'document', 'book'),
(75, 'Domain-Driven Design in PHP', '{"author":"Carlos Buenosvinos","publication_details":"Packt Publishing c.2016","call_number":"CP05 DDDP 2016"}', 2, 0, 'document', 'book'),
(76, 'How to be a Graphic Designer, Without Losing Your Soul', '{"author":"Adrian Shaughnessy","publication_details":"Laurence King Pub. c.2005","call_number":"BD01 GDWLS 2005"}', 2, 0, 'document', 'book'),
(77, 'Thinking with Type', '{"author":"Ellen Lupton","publication_details":"Princetion Architectural Press c.2004","call_number":"BD02 TWT 2004"}', 1, 0, 'document', 'book'),
(78, 'The Elements of Typographic Style', '{"author":"Robert Bringhurst","publication_details":"Hartley & Marks Pub. c.1992","call_number":"BD03 EOT 1992"}', 3, 0, 'document', 'book'),
(79, 'Just My Type', '{"author":"Simon Garfield","publication_details":"Gotham Books c.2010","call_number":"BD04 JMT 2010"}', 2, 0, 'document', 'book'),
(80, 'The Elements of Graphic Design', '{"author":"Alex White","publication_details":"Allworth Press c.2002","call_number":"BD05 EGD 2002"}', 8, 0, 'document', 'book'),
(81, 'The Social Science Journal', '{"author":"Stephanie Witt","publication_details":"Elsevier c.1976","call_number":"JS01 SSC 1976"}', 2, 0, 'document', 'journal'),
(82, 'Behavioral and Brain Sciences', '{"author":"Paul Bloom","publication_details":"Cambridge University Press c.1978","call_number":"JS02 BBS 1978"}', 2, 0, 'document', 'journal'),
(83, 'Journal of Fluid Mechanics', '{"author":"Grae Worster","publication_details":"Cambridge University Press c.1956","call_number":"JS03 JFM 1956"}', 2, 0, 'document', 'journal'),
(84, 'Nano Letters', '{"author":"Paul Alivisatos","publication_details":"American Chemical Society c.2001","call_number":"JS04 NL 2001"}', 3, 2, 'document', 'journal'),
(85, 'Journal of Statistical Software', '{"author":"Achim Zeileis","publication_details":"Foundation for Open Access Statistics c.1996","call_number":"JP01 JSS 1996"}', 1, 0, 'document', 'journal'),
(86, 'Times Higher Education Vol. 10 Issue 4', '{"author":"John Gill","publication_details":"TES Global c.1971","call_number":"MG01 THEV10I4 1971"}', 1, 0, 'document', 'magazine'),
(87, 'SchoolArts Vol. 5 Issue 1', '{"author":"Nancy Walkup","publication_details":"Wyatt Wade c.1901","call_number":"MG02 SAV5I1 1901"}', 1, 0, 'document', 'magazine'),
(88, 'Education Times Vol. 7 Issue 3', '{"author":"Indu Jain","publication_details":"Bennett, Coleman & Co Ltd. c.1995","call_number":"MG03 ETV7I3 1995"}', 2, 0, 'document', 'magazine'),
(89, 'Harvard Magazine Vol. 10 Issue 7', '{"author":"John S. Rosenberg","publication_details":"Harvard Magazine Inc c.1898","call_number":"MG04 HMV10I7 1898"}', 1, 0, 'document', 'magazine'),
(90, 'Science News Vol. 12 Issue 8', '{"author":"Eva Emerson","publication_details":"Society for Science & the Public c.1922","call_number":"MG SNV12I8 1922"}', 5, 0, 'document', 'magazine'),
(91, 'The Manila Bulletin 06/24/17', '{"author":"Dr. Crispulo J. Icban, Jr.","publication_details":"Manila Bulletin Publishing Corporation c.1900","call_number":"NG TMB062417"}', 1, 0, 'document', 'newspaper'),
(92, 'The Manila Bulletin 03/12/18', '{"author":"Dr. Crispulo J. Icban, Jr.","publication_details":"Manila Bulletin Publishing Corporation c.1900","call_number":"NG TMB031218"}', 3, 0, 'document', 'newspaper'),
(93, 'The Manila Bulletin 05/01/18', '{"author":"Dr. Crispulo J. Icban, Jr.","publication_details":"Manila Bulletin Publishing Corporation c.1900","call_number":"NG TMB050118"}', 1, 0, 'document', 'newspaper'),
(94, 'The Philippine Star 12/03/18', '{"author":"Ana Marie Pamintuan","publication_details":"Philippine Star Printing Co., Inc. c.1986","call_number":"NG TPS120318"}', 5, 0, 'document', 'newspaper'),
(95, 'The Philippine Star 3/28/17', '{"author":"Ana Marie Pamintuan","publication_details":"Philippine Star Printing Co., Inc. c.1986","call_number":"NG TPS032817"}', 1, 0, 'document', 'newspaper'),
(96, 'Change: A Hybrid Animation Film in a Database Driven Website', '{"author":"La Prisma","publication_details":"FEU TECH Pub. c.2018","call_number":"TF04 CHDDW 2018"}', 1, 0, 'document', 'thesis'),
(97, 'Architectural Styles and the Design of Network-based Software Architectures', '{"author":"The Uncalled Four","publication_details":"FEU TECH Pub. c.2016","call_number":"TF03 ASDNSA 2016"}', 1, 0, 'document', 'thesis'),
(98, 'Development of Fantasy Football Website', '{"author":"Chamber of Secrets","publication_details":"FEU TECH Pub. c.2013","call_number":"TF01 DFFW"}', 1, 1, 'document', 'thesis'),
(99, 'Secure Corporate Network Infrastructure Development', '{"author":"The Herd","publication_details":"FEU TECH Pub. c.2014","call_number":"TF02 SCND 2014"}', 1, 0, 'document', 'thesis'),
(100, 'Virtual Reality Applications and Universal Accessibility', '{"author":"Fantastic 4","publication_details":"FEU TECH Pub. c.2014","call_number":"TF05 VRAUA 2014"}', 1, 1, 'document', 'thesis'),
(101, 'Fragmented Podcast', '{"type":"mp3","description":"Android Development Audio Book","duration":"02:42:56","file_size":"50mb"}', 0, 0, 'digital', 'audio'),
(102, 'The Complete Software Developer’s Career Guide', '{"type":"mp3","description":"Software Developer Career Guide Audio Book","duration":"1:54:19","file_size":"24mb"}', 0, 0, 'digital', 'audio'),
(103, 'Algorithms to Live By', '{"type":"wav","description":"Programming Algorithms Audio Book","duration":"54:29","file_size":"12mb"}', 0, 0, 'digital', 'audio'),
(104, 'Change by Design', '{"type":"wma","description":"Graphic Design Audio Book","duration":"2:13:54","file_size":"30mb"}', 0, 0, 'digital', 'audio'),
(105, 'How to Think Like a Great Graphic Designer', '{"type":"wav","description":"Approaches, Processes and Opinions in Designing Audio Book","duration":"58:45","file_size":"8mb"}', 0, 0, 'digital', 'audio'),
(106, 'Introduction to Programming', '{"type":"mp4","description":"Introduction to the Basics of Programming","duration":"32:46","file_size":"100mb"}', 0, 0, 'digital', 'video'),
(107, 'C# Tutorial - Full Course for Beginners', '{"type":"wmv","description":"C# Full Tutorial","duration":"4:31:09","file_size":"160mb"}', 0, 0, 'digital', 'video'),
(108, 'Java Programming', '{"type":"avi","description":"Introduction to Java","duration":"34:30","file_size":"86mb"}', 0, 0, 'digital', 'video'),
(109, 'Beginning Graphic Design: Fundamentals', '{"type":"mov","description":"Introduction to Graphic Design","duration":"6:26","file_size":"40mb"}', 0, 0, 'digital', 'video'),
(110, 'Beginning Graphic Design: Typography', '{"type":"mp4","description":"Basic Typography Tutorial","duration":"6:24","file_size":"65mb"}', 0, 0, 'digital', 'video'),
(111, 'Object-Oriented JavaScript', '{"type":"pdf","file_size":"35mb","author":"Stoyan Stefanov","publication_details":"Packt Publishing c.2008"}', 0, 0, 'digital', 'ebook'),
(112, 'Thinking in Java', '{"type":"pdf","author":"Bruce Eckel","publication_details":"Prentice Hall c.2006","file_size":"15mb"}', 0, 0, 'digital', 'ebook'),
(113, 'Head First Java', '{"type":"docx","author":"Kathy Sierra","publication_details":"O''Reilly Media, Inc. c.2003","file_size":"12mb"}', 0, 0, 'digital', 'ebook'),
(114, 'Learning PHP and MySQL', '{"type":"docx","author":"Michele E. Davis","publication_details":"O''Reilly Media, Inc. c.2006","file_size":"28mb"}', 0, 0, 'digital', 'ebook'),
(115, 'Domain-Driven Design in PHP', '{"type":"pdf","author":"Carlos Buenosvinos","publication_details":"Packt Publishing c.2016","file_size":"35mb"}', 0, 0, 'digital', 'ebook'),
(116, 'Adobe Master Collection CC 2017', '{"type":"exe","file_size":"17.4gb","version":"1"}', 0, 0, 'digital', 'software'),
(117, 'PacketTracer71_64bit_setup_signed', '{"type":"exe","version":"2.1","file_size":"128mb"}', 0, 0, 'digital', 'software'),
(118, 'Dev-Cpp 5.7.1 TDM-GCC x64 4.8.1 Setup', '{"type":"exe","version":"5.7.1","file_size":"44.8mb"}', 0, 0, 'digital', 'software'),
(119, 'mysql-installer-community-8.0.13.0', '{"type":"msi","version":"8.0.13.0","file_size":"313mb"}', 0, 0, 'digital', 'software'),
(120, 'netbeans-8.0.2-windows', '{"type":"exe","file_size":"204mb","version":"8.0.2"}', 0, 0, 'digital', 'software'),
(121, 'Notepad++ Installer', '{"type":"exe","file_size":"3.91mb","version":"6.8.6"}', 0, 0, 'digital', 'software'),
(122, 'Oracle 11g Express Installer', '{"type":"zip","version":"11.0.1","file_size":"312mb"}', 0, 0, 'digital', 'software'),
(123, 'SQL Developer Installer', '{"type":"zip","version":"3.1.9","file_size":"313mb"}', 0, 0, 'digital', 'software'),
(124, 'xampp-win32-5.6.19-0-VC11-installer', '{"type":"exe","version":"5.6.19","file_size":"107mb"}', 0, 0, 'digital', 'software'),
(125, 'VSCodeUserSetup-x64-1.30.0', '{"type":"zip","version":"1.30.0","file_size":"43.8mb"}', 0, 0, 'digital', 'software'),
(131, 'A Bibliography of Philippine Literature', '{"author":"Perez, Cirilo B.","publication_details":"1916 ; Manila","call_number":"( Bi 012 P538a 1960)"}', 3, 0, 'document', 'book'),
(141, ' La imprenta en Manila : desde sus origines hasta 1800 ; adiciones y ampliaciones', '{"author":"Medina, Jose Toribio","publication_details":"1904 - Impreso y grabado en casa del autor","call_number":"(Fil. 015.914 M458i 1904)"}', 1, 0, 'document', 'book'),
(151, 'Filipino writers in English', '{"author":"Clarino, Jose V.","publication_details":"Manila : General Printing Press, 1931","call_number":"( Fil 920 C544f 1931)"}', 2, 0, 'document', 'book'),
(161, 'Index to the Philippine journal of science', '{"author":"Lira, Angel Y.","publication_details":"1963 - Bureau of Printing","call_number":"( Fil 016.605 L67i 1963)"}', 1, 0, 'document', 'book'),
(171, 'A Survey of Philippine bibliographical literature : 1900-1950, a preliminary checklist', '{"author":"Manuel, Esperidion Arsenio.\\t","publication_details":"Manuel, Esperidion Arsenio.\\t","call_number":"( Fil 016.89921 M319s 1950)"}', 5, 0, 'document', 'book'),
(181, 'Union catalog of Philippine materials of sixty-four government agency libraries of the Philippines', '{"author":"Philippines. University, Manila. Institute of Public Administration. Inter-Department Reference Service.","publication_details":"Manila : The Institute, 1962","call_number":"( PL 016.9914 P538u)"}', 2, 0, 'document', 'book'),
(191, 'Tagalog periodical literature', '{"author":"Agoncillo, Teodoro A.","publication_details":"1953 - Institute of National Language, ; Manila","call_number":"( Fil 010.89921 Ag73t 1953)"}', 3, 0, 'document', 'book'),
(201, 'Union catalog on Philippine culture : literature', '{"author":"Agoncillo, Teodoro A.","publication_details":"1996 - CCP Library, ; Manila","call_number":"( PL 016.808 C889u 1996)"}', 3, 0, 'document', 'book'),
(211, 'Edsa 2', '{"author":"Coronel, Sheila","publication_details":"Pasig City : Anvil Publishing, c2001","call_number":"Fil DS686.913 C6E37"}', 6, 0, 'document', 'journal'),
(221, 'General Santos City', '{"author":"Campado, Andrea V.","publication_details":"[S.l.] : [s.n.], c1987","call_number":"Fil DS686 G4C3"}', 3, 0, 'document', 'journal'),
(231, 'Notes on the Filipino action Film', '{"author":"Sotto, Agustin L.","publication_details":"[S.l.] : [s.n.], c1987","call_number":"Fil PN1995.9 A2S6"}', 3, 0, 'document', 'journal'),
(241, 'A Spaniard in Aguinaldo''s Army', '{"author":"Perez, Telesforo Carasco Y.","publication_details":"Metro Manila : Solar Publishing, c1986","call_number":"Fil DS676.8 P4P4"}', 3, 0, 'document', 'journal'),
(251, 'Testament from a prison cell', '{"author":"Aquino Jr., Benigno S.","publication_details":"Los Angeles, California : Philippine Journal, c1988","call_number":"Fil DS686.6 A66T4"}', 5, 0, 'document', 'journal'),
(261, 'Unang pag-ibig', '{"author":"Florentino, Alberto S.","publication_details":"Manila : U.P. Press, c1979","call_number":"Fil PL5539 U5L5"}', 3, 0, 'document', 'journal'),
(271, 'Huling habilin', '{"author":"Florentino, Alberto S.","publication_details":"Manila : U.P. Press, c1979","call_number":"Fil PL5539 U5L5"}', 1, 0, 'document', 'journal'),
(281, 'Hua-hua non pageh', '{"author":"Dalagdig, Esteban D.","publication_details":"Nueva Vizcaya, c1976","call_number":"Fil GT4481 D3"}', 2, 0, 'document', 'journal'),
(291, 'Movie journal; the rise of the new American cinema 1959-1971', '{"author":"Mekas, Jonas","publication_details":"New York : Collier Books, c1971","call_number":"LOB PN1993.5 U6M4"}', 2, 0, 'document', 'journal'),
(301, 'Juan Luna Y Novicio first internationally known Filipino painter', '{"author":"Da Silva, Carlos E.","publication_details":"Manila : National Historical Institute, c1977","call_number":"Fil ND1029 L8D45"}', 4, 0, 'document', 'journal'),
(311, 'Metamorphosis of Filipino values in fantasy films', '{"author":"Landicho, Celeste Grace S.","publication_details":"[s.l.] : [s.n.], c1999","call_number":"Fil PN1995.9 S6L3"}', 1, 0, 'document', 'thesis'),
(321, 'Ang semiotika ng anting-anting', '{"author":"Pambid, Nenita D.","publication_details":"[s.l.] : [s.n.], c1989","call_number":"Fil GR600 P3"}', 1, 0, 'document', 'thesis'),
(331, 'The Cultural Center of the Philippines and its musical activities', '{"author":"Puertollano, Rosa L.","publication_details":"[s.l.] : [s.n.], c1983","call_number":"Fil NX820 P5P8"}', 1, 0, 'document', 'thesis'),
(341, 'The zarzuela as art form in the plays of Hermogenes Ilagan', '{"author":"Carpio, Rustica C.","publication_details":"Manila : University of Santo Tomas, c1979","call_number":"Fil PN 3203 C3"}', 1, 0, 'document', 'thesis'),
(351, 'Bibliography of thesis and dissertations on Muslim Filipinos', '{"author":"Columnas, Edelin L.","publication_details":"Makati : Filipinas Foundation, c1976","call_number":"RFil Z5053 C6"}', 1, 0, 'document', 'thesis'),
(361, 'The graduate thesis', '{"author":"Sugden, Virginia M.","publication_details":"New York : Pitman, c1973","call_number":"LB2369 S8"}', 1, 0, 'document', 'thesis'),
(371, 'Seventy years of Philippine theater ', '{"author":"Dizon, Fe S.","publication_details":"Quezon City : University of the Philippines, c1972","call_number":"RFil Z3298 L5D5"}', 1, 0, 'document', 'thesis'),
(381, 'The dragon slayers of the countryside ', '{"author":"Maslog, Crispin C.","publication_details":"Manila : Philippine Press Institute, c1989","call_number":"Fil PN5429 P5M3"}', 1, 0, 'document', 'journal'),
(391, 'Philippine Daily Inquirer; Sandiganbayan junks Iloilo solon’s plea on graft case', '{"author":"Nestor P. Burgos Jr.","publication_details":"Philippine Daily Inquirer - Raul Pangalanan 02/04/19","call_number":"Fil PN6039 / P8M1"}', 1, 0, 'document', 'newspaper'),
(401, 'Philippine Daily Inquirer; PPA offers lot for families displaced by Manila Bay cleanup', '{"author":"DJ Yap","publication_details":"Philippine Daily Inquirer - Raul Pangalanan 02/04/19","call_number":"Fil PN8080 / P9M3"}', 1, 0, 'document', 'newspaper'),
(411, 'Philippine Daily Inquirer; De Lima wants Noynoy, Bato to testify on her drug cases', '{"author":"Dexter Cabalza","publication_details":"Philippine Daily Inquirer - Raul Pangalanan 02/02/19","call_number":"Fil PN7015 / P2M4"}', 1, 0, 'document', 'newspaper'),
(421, 'Philippine Daily Inquirer; 3 killed in 2 drug operations', '{"author":"Nestle L. Semilla","publication_details":"Philippine Daily Inquirer - Raul Pangalanan 02/01/19","call_number":"Fil PN3354 / P0M6"}', 0, 0, 'document', 'newspaper'),
(431, 'Philippine Daily Inquirer; Sotto ‘tired’ of pork issues, threatens to reenact 2018 budget', '{"author":"Marlon Ramos","publication_details":"Philippine Daily Inquirer - Raul Pangalanan 01/31/19","call_number":"Fil PN0728 / P1M7"}', 1, 0, 'document', 'newspaper');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(30) NOT NULL,
  `middle_initial` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `type` varchar(20) NOT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=27;

--
-- Dumping data for table `users`
--

REPLACE INTO `users` (`id`, `first_name`, `middle_initial`, `last_name`, `email`, `username`, `password`, `type`, `status`) VALUES
(24, 'James', 'Johnson', 'Smith', 'john.smith@example.com', '201400000', 'bb40053ec184f2f58bf5cd61fe9b1cb20682e7f669f1bc62d92926571b75c39dc3a430c8fce421df8971f099f019318e1b01df2325e650fe6ffe98ab3d7927327cd96332ce', 'administrator', 'enrolled'),
(25, 'Jane', 'Williams', 'Doe', 'jane.doe@example.com', '201400001', 'bb40053ec184f2f58bf5cd61fe9b1cb20682e6f669f1bc62d92926571b75c39dc3a430c8fce421df8971f099f019318e1b01df2325e650fe6ffe98ab3d7927327cd96332ce', 'librarian', 'enrolled'),
(26, 'John', 'Brown', 'Doe', 'john.doe@example.com', '201400002', 'bb40053ec184f2f58bf5cd61fe9b1cb20682e5f669f1bc62d92926571b75c39dc3a430c8fce421df8971f099f019318e1b01df2325e650fe6ffe98ab3d7927327cd96332ce', 'student', 'enrolled');
