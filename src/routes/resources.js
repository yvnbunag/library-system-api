import express from 'express'
import importControllers from '@controllers/importControllers'
import importMiddlewares from '@middlewares/importMiddlewares'

const router = express.Router()

/* Initialize import classes. */
const controllers = new importControllers('resources')
const middlewares = new importMiddlewares()

/**
 * Returns short API name.
 * @name get/api
 */
router.get(
  '/api',
  (req, res) => {
    res.send('Library-System Resources API')
  }
)

/* Validation of user token. */
router.use(middlewares.use('userValidationMiddleware').validate)

/**
 * Read a set of resources.
 * @name get/
 */
router.get(
  '/',
  controllers.use('resourcesDatabaseController').getAll
)

/**
 * Read a set of resources from a search query.
 * @name get/search
 */
router.get(
  '/search',
  controllers.use('resourcesDatabaseController').search
)

/**
 * Generate excel report file for all resources in a query.
 * @name get/generate
 */
router.get(
  '/generate',
  middlewares.use('userValidationMiddleware').validateAdminCredential,
  controllers.use('resourcesDatabaseController').generateReport
)

/**
 * Read an individual resource details.
 * @name get/:id
 */
router.get(
  '/:id',
  controllers.use('resourcesDatabaseController').get
)

/* Validation of user administrative credentials. */
router.use(middlewares.use('userValidationMiddleware').validateAdminCredential)

/**
 * Create a resource.
 * @name put/
 */
router.put(
  '/',
  controllers.use('resourcesDatabaseController').create
)

/**
 * Update a resource.
 * @name put/:id
 */
router.post(
  '/:id',
  controllers.use('resourcesDatabaseController').update
)

/**
 * Delete a resource.
 * @name delete/:id
 */
router.delete(
  '/:id',
  controllers.use('resourcesDatabaseController').delete
)

export default router
