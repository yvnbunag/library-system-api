/* eslint-disable */
import express from 'express'
const router = express.Router()

/**
 * Initial endpoint of the API.
 * @name get/
 */
router.get('/', function(req, res) {
  res.send(`Library System API - NodeJS v${process.versions.node} ` +
    `Express v${require('express/package').version}`
  )
})

export default router
