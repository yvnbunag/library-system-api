import express from 'express'
import importControllers from '@controllers/importControllers'
import importMiddlewares from '@middlewares/importMiddlewares'

const router = express.Router()

/* Initialize import classes. */
const controllers = new importControllers('users')
const middlewares = new importMiddlewares()

/**
 * Returns short API name.
 * @name get/api
 */
router.get(
  '/api',
  (req, res) => {
    res.send('Library-System Users API')
  }
)

/**
 * Read user credentials.
 * @name get/settings
 */
router.get(
  '/settings',
  middlewares.use('userValidationMiddleware').validate,
  controllers.use('usersDatabaseController').settings
)

/* Validation of external API token. */
router.use(middlewares.use('apiValidationMiddleware').validate)

/**
 * Import users from excel file by performing create/update operations.
 * @name put/import
 */
router.put(
  '/import',
  controllers.use('usersAutomationController').import
)

/**
 * Export all users into an excel file.
 * @name get/export
 */
router.get(
  '/export',
  controllers.use('usersAutomationController').export
)

/**
 * Create a user.
 * @name put/
 */
router.put(
  '/',
  controllers.use('usersDatabaseController').create
)

/**
 * Read a set of resources.
 * @name get/
 */
router.get(
  '/',
  controllers.use('usersDatabaseController').getAll
)

/**
 * Read a set of resources from a search query.
 * @name get/search
 */
router.get(
  '/search',
  controllers.use('usersDatabaseController').search
)

/**
 * Read an individual user details.
 * @name get/:id
 */
router.get(
  '/:id',
  controllers.use('usersDatabaseController').get
)

/**
 * Update an individual user details.
 * @name post/:id
 */
router.post(
  '/:id',
  controllers.use('usersDatabaseController').update
)

/**
 * Delete a user.
 * @name delete/:id
 */
router.delete(
  '/:id',
  controllers.use('usersDatabaseController').delete
)

export default router
