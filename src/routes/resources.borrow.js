import express from 'express'
import importControllers from '@controllers/importControllers'
import importMiddlewares from '@middlewares/importMiddlewares'

const router = express.Router()

/* Initialize import classes. */
const controllers = new importControllers('resources')
const middlewares = new importMiddlewares()

/**
 * Returns short API name.
 * @name get/api
 */
router.get(
  '/api',
  (req, res) => {
    res.send('Library-System Borrowing API')
  }
)

/* Validation of user token. */
router.use(middlewares.use('userValidationMiddleware').validate)

/**
 * User borrow request creation.
 * @name put/users/:user_id/request/:resource_id
 */
router.put(
  '/users/:user_id/request/:resource_id',
  controllers.use('borrowDatabaseController').createBorrowRequest
)

/**
 * Read user borrow requests.
 * @name get/users/:user_id
 */
router.get(
  '/users/:user_id',
  controllers.use('borrowDatabaseController').getBorrowRequests
)

/**
 * Read user borrow history.
 * @name get/users/:user_id/history
 */
router.get(
  '/users/:user_id/history',
  controllers.use('borrowDatabaseController').getBorrowHistory
)

/**
 * Delete user borrow request from manual cancel.
 * @name delete/users/:user_id/cancel/:borrow_id
 */
router.delete(
  '/users/:user_id/cancel/:borrow_id',
  controllers.use('borrowDatabaseController').cancelBorrowRequest
)

/* Validation of user administrative credentials. */
router.use(middlewares.use('userValidationMiddleware').validateAdminCredential)

/**
 * Read pending borrow requests.
 * @name get/pending
 */
router.get(
  '/pending',
  controllers.use('borrowManagementController').getPending
)

/**
 * Read pending borrow requests from search query.
 * @name get/pending/search
 */
router.get(
  '/pending/search',
  controllers.use('borrowManagementController').searchPending
)

/**
 * Read active borrow requests.
 * @name get/lent
 */
router.get(
  '/lent',
  controllers.use('borrowManagementController').getLent
)

/**
 * Read active borrow requests from search query.
 * @name get/lent/search
 */
router.get(
  '/lent/search',
  controllers.use('borrowManagementController').searchLent
)

/**
 * Read active request histories.
 * @name get/history
 */
router.get(
  '/history',
  controllers.use('borrowManagementController').getHistory
)

/**
 * Read active request histories from search query.
 * @name get/history/search
 */
router.get(
  '/history/search',
  controllers.use('borrowManagementController').searchHistory
)

/**
 * Update borrow request.
 * @name post/:borrow_id
 */
router.post(
  '/:borrow_id',
  controllers.use('borrowManagementController').updateBorrow
)

export default router
