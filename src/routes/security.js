/* eslint no-undef: off */
import express from 'express'
import importControllers from '@controllers/importControllers'
import importMiddlewares from '@middlewares/importMiddlewares'

const router = express.Router()

/* Initialize import classes. */
const controllers = new importControllers('security')
const middlewares = new importMiddlewares()

/**
 * Returns short API name.
 * @name get/api
 */
router.get(
  '/api',
  (req, res) => {
    res.send('Library-System Security API')
  }
)

/* Validation of user token. */
router.use(middlewares.use('apiValidationMiddleware').validate)

/**
 * Authenticates user credentials and returns an access token on success.
 * @name post/authenticate
 */
router.post(
  '/authenticate',
  controllers.use('userAuthenticationController').index
)

export default router
