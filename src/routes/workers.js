/* eslint no-undef: off */
import express from 'express'
import importMiddlewares from '@middlewares/importMiddlewares'

const router = express.Router()

/* Initialize import classes. */
const middlewares = new importMiddlewares()

/**
 * Returns short API name.
 * @name get/api
 */
router.get(
  '/api',
  (req, res) => {
    res.send('Library-System Workers API')
  }
)

/* Validation of external API token. */
router.use(middlewares.use('apiValidationMiddleware').validate)

/**
 * Starts workers to do scheduled jobs on a given time interval.
 * @name post/start
 */
router.post(
  '/start',
  (req, res) => {
    if (!worker.workerStatus()) {
      worker.startWorker()
      res.send({
        success: true,
        reason: 'Worker initialized'
      })
    } else {
      res.send({
        success: false,
        reason: 'Worker already running'
      })
    }
  }
)

/**
 * Stops workers.
 * @name post/stop
 */
router.post(
  '/stop',
  (req, res) => {
    if (worker.workerStatus()) {
      worker.stopWorker()
      res.send({
        success: true,
        reason: 'Worker stopped'
      })
    } else {
      res.send({
        success: false,
        reason: 'Worker is not running'
      })
    }
  }
)

/**
 * Returns current status of workers.
 * @name post/status
 */
router.post(
  '/status',
  (req, res) => {
    let status = 'Active'
    var lastRun = 'Workers are not run since last application start'

    if (!worker.workerStatus()) {
      status = 'Stopped'
    }

    if (worker.lastRun) {
      lastRun = worker.lastRunFormatted()
    }

    const response = {
      success: true,
      details: {
        status,
        lastRun
      }
    }

    if (status === 'Active') {
      response.details.nextRun = worker.nextRunFormatted()
      response.details.runInterval = `${worker.runInterval} minute/s`
    }

    res.send(response)
  }
)

/**
 * Manually runs worker jobs.
 * @name post/run
 */
router.post(
  '/run',
  async (req, res) => {
    const message = await worker.executeJobs(true)

    res.send({
      success: true,
      result: message
    })
  }
)

export default router
