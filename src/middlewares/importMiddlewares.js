/*eslint-disable */

/**
 * Class used by routers to dynamically import middlewares.
 *
 * @class importMiddlewares
 */
export default class importMiddlewares {

  /**
   * Called from the router to use the middleware needed.
   *
   * @method use
   * @param {String} middlewareName name of the middleware to be returned
   * @return {Object} returns new middlewareClass()
   */
  use(middlewareName) {
    const middlewareClass = 
      require(`@middlewares/${middlewareName}`).default
    return new middlewareClass()
  }
}
