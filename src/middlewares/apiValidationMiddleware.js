/* eslint max-statements: ["error", 15] */
import baseMiddleware from '@middlewares/baseMiddleware'

/**
 * Middleware class for external API validation.
 *
 * @class apiValidationMiddleware
 * @constructor
 */
export default class apiValidationMiddleware extends baseMiddleware {
  constructor() {
    super()
    // Binding of class methods(to be used publicly)
    this.validate = this.validate.bind(this)
  }

  /**
   * Validates access token from external API.
   *
   * @method validate
   * @param {Object} req request object containing:
   *  @prop {String} token access token required for access to the API
   * @param {Object} res object that will send the response back to its origin
   *  and will be passed to the next handler if validation succeeds
   * @param {Function} next function that will pass/skip the execution to the
   *  next middleware/handler
   * @return {null} returns null on success
   */
  validate(req, res, next) {
    try {
      const { token } = req

      if (!token) {
        errorHandler.throw('API access token required', 403)
      }

      if (token !== process.env.API_ACCESS_REGISTERED_MOCK_TOKEN) {
        errorHandler.throw('API not registered', 403)
      }

    } catch (err) {
      errorHandler.handle(err, req, res)
    }
    next()
  }
}
