/* eslint max-statements: ["error", 15] */
import baseMiddleware from '@middlewares/baseMiddleware'
import jwt from 'jsonwebtoken'

/**
 * Middleware class for validation of user's access token.
 *
 * @class userValidationMiddleware
 * @constructor
 */
export default class userValidationMiddleware extends baseMiddleware {
  constructor() {
    super()
    // Binding of class methods(to be used publicly)
    this.validate = this.validate.bind(this)
    this.validateAdminCredential = this.validateAdminCredential.bind(this)
  }

  /**
   * Validates access token from users that before permitting access to the
   *  system micro-services. Stores user credentials into the `req` object.
   *
   * @method validate
   * @param {Object} req request object containing:
   *  @prop {String} token access token required for access to the API
   * @param {Object} res object that will send the response back to its origin
   *  and will be passed to the next handler if validation succeeds
   * @param {Function} next function that will pass/skip the execution to the
   *  next middleware/handler
   * @return {null} returns null on success
   */
  async validate(req, res, next) {
    try {
      const { token } = req

      if (!token) {
        errorHandler.throw('unauthenticated', 403)
      }

      jwt.verify(token, process.env.JWT_PASSPHRASE, (err, dec) => {
        if (err) {
          errorHandler.throw(err.message, 403)
        }
        req.tokenDetails = dec
      })

      const User = this.initializeModel('usersModel')

      const user = await User.middlewareValidate(
        req.tokenDetails.id,
        req.tokenDetails.username,
        req.tokenDetails.password
      )

      if (!user) {
        errorHandler.throw('authentication mismatch', 401)
      }

      if (!this.validateStatusCredential(user.status)) {
        errorHandler.throw('status credential revoked', 403)
      }

      Reflect.deleteProperty(user, 'password')
      req.user = user
    } catch (err) {
      errorHandler.handle(err, req, res)
    }
    next()
  }

  /**
   * Validates status credential of the user.
   *
   * @method validateStatusCredential
   * @param {String} status current status of a user's account
   * @return {Boolean} returns statusCredential
   */
  validateStatusCredential(status) {
    const credentials = this.getCredentials()
    let statusCredential = 'Invalid'

    credentials.status.allowed.forEach((value) => {
      if (value === status) {
        statusCredential = true
      }
    })

    credentials.status.prohibited.forEach((value) => {
      if (value === status) {
        statusCredential = false
      }
    })

    if (typeof statusCredential !== 'boolean') {
      errorHandler.throw('Invalid user status', 500)
    }

    return statusCredential
  }

  /**
   * Validates admin credentials for micro-services that require administrative
   *  privileges.
   *
   * @method validateAdminCredential
   * @param {Object} req request object containing:
   *  @prop {Object} user contains the credentials of the user
   * @param {Object} res object that will send the response back to its origin
   *  and will be passed to the next handler if validation succeeds
   * @param {Function} next function that will pass/skip the execution to the
   *  next middleware/handler
   * @return {null} returns null on success
   */
  validateAdminCredential(req, res, next) {
    const credentials = this.getCredentials()
    const userType = req.user.type
    let adminTypeCredential = false

    credentials.type.admin.forEach((value) => {
      if (value === userType) {
        adminTypeCredential = true
      }
    })

    if (!adminTypeCredential) {
      errorHandler.throw('You do not have admin credentials', 403)
    }

    next()
  }
}
