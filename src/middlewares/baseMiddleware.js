/* eslint-disable */
import YmlLoader from '@lib/ymlLoader'

/**
 * Superclass for middlewares.
 *
 * @class baseMiddleware
 */
export default class baseMiddleware {
  /**
   * Instantiates models that would be used by child classes.
   *
   * @method initializeModel
   * @param {String} modelName name of the model to be instantiated
   * @return {Object} returns model
   */
  initializeModel(modelName) {
    const model = require(`@models/${modelName}`).default
    
    return new model
  }

  /**
   * Extracts parameter from parameters config file.
   *
   * @method getParameters
   * @param {String} paramName name of the parameter to be extracted
   * @return {String} returns parameter value
   */
  getParameters(paramName){
    const parameters = new YmlLoader().loadGlobalConfig('parameters')
    return parameters[paramName]
  }

  /**
   * Reads credentials mapping from config file.
   *
   * @method getCredentials
   * @return {Object} returns credentials
   */
  getCredentials(){
    const credentials = new YmlLoader().loadGlobalConfig('credentials.mapping')
    return credentials
  }
}
