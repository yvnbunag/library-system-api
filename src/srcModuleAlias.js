/*  eslint no-undef: "off"*/
import path from 'path'
import moduleAlias from 'module-alias'

/*
 * Sets aliases for directory namespace.
 */
moduleAlias.addAliases({
  '@controllers': path.resolve(__dirname, './controllers'),
  '@models': path.resolve(__dirname, './domain/models'),
  '@lib': path.resolve(__dirname, './lib'),
  '@middlewares': path.resolve(__dirname, './middlewares'),
  '@workers': path.resolve(__dirname, './workers')
})


export default moduleAlias
