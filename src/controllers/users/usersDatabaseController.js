/* eslint max-statements: ["error", 20] */
import baseController from '@controllers/baseController'
import QueryBuilder from '@lib/sequelize/queryBuilder'
import CipherManager from '@lib/bcrypt/cipherManager'

/**
 * Controller class for users CRUD database operations.
 *
 * @class usersDatabaseController
 * @constructor
 */
export default class usersDatabaseController extends baseController {
  constructor() {
    super()
    // Binding of class methods(to be used publicly)
    this.create = this.create.bind(this)
    this.get = this.get.bind(this)
    this.getAll = this.getAll.bind(this)
    this.search = this.search.bind(this)
    this.update = this.update.bind(this)
    this.delete = this.delete.bind(this)
    this.settings = this.settings.bind(this)
    // Initialization of models
    this.User = this.initializeModel('usersModel')
    // Initialization of artifacts
    this.queryBuilder = new QueryBuilder()
    this.cipherManager = new CipherManager()
  }

  /**
   * Reads user settings.
   *
   * @method settings
   * @param {Object} req request object containing:
   *  @prop {Object} user user credentials injected by userValidationMiddleware
   * @param {Object} res object that will send the response back to its origin
   * @return {null} returns null on success
   */
  async settings(req, res) {
    try {
      const borrowQueueCount = await this.getUserBorrowQueueCount(req.user.id)
      var userDetails = req.user

      Object.assign(userDetails, borrowQueueCount)

      res.send({
        success: true,
        data: userDetails
      })
    } catch (err) {
      errorHandler.handle(err, req, res)
    }
  }

  /**
   * Reads user borrow queue count aggregate.
   *
   * @method getUserBorrowQueueCount
   * @param {string} user_id id of user to get borrow queue count from
   * @return {Object} returns borrowCounts
   */
  async getUserBorrowQueueCount(user_id) {
    const Borrow = this.initializeModel('borrowModel')
    var borrowCounts = {}
    var borrowPending = 0
    var borrowLent = 0
    var borrowLentDueDates = []

    const checkCountQuery = this.queryBuilder.buildBorrowOrQuery(
        user_id,
        [
          {
            status: 'pending'
          },
          {
            status: 'lent'
          }
        ]
      )
      const borrows = await Borrow.getAll(
        1,
        10,
        checkCountQuery
      )

      borrows.rows.forEach((value) => {
        if (value.status === 'pending') {
          borrowPending += 1
        } else {
          borrowLentDueDates.push(value.date_due)
          borrowLent += 1
        }
      })

      borrowCounts.borrows = borrows.count
      borrowCounts.pending = borrowPending
      borrowCounts.lent = borrowLent
      borrowCounts.borrowLentDueDates = borrowLentDueDates

      return borrowCounts
  }

  /**
   * Creates users.
   *
   * @method create
   * @param {Object} req request object containing:
   *  @prop {Object} body JSON body parameters containing create user operation
   *    variables
   * @param {Object} res object that will send the response back to its origin
   * @return {null} returns null on success
   */
  async create(req, res) {
    try {
       this.queryBuilder.validateBody(
        this.User.modelMappingToArray({ password: true }),
        req.body,
        {
          requireAll: true
        }
      )

      if (!this.validateEmail(req.body.email)) {
        errorHandler.throw('The email address provided is invalid', 400)
      }

      await this.checkExistingUsername(req.body.username)

      req.body.password = this.cipherManager.encrypt(req.body.password)
      const user = await this.User.create(req.body)

      if (!user) {
        errorHandler.throw('Failed to create user', 500)
      }

      res.send({
        success: true,
        data: this.sanitizeUserData(user)
      })
    } catch (err) {
      errorHandler.handle(err, req, res)
    }
  }

  /**
   * Reads user data.
   *
   * @method get
   * @param {Object} req request object containing:
   *  @prop {Object} params URL parameters containing:
   *    @prop {String} id user id
   * @param {Object} res object that will send the response back to its origin
   * @return {null} returns null on success
   */
  async get(req, res) {
    try {
      const { id } = req.params
      const user = await this.User.get({ id })

      if (!user) {
        errorHandler.throw('User does not exist', 404)
      }

      res.send({
        success: true,
        data: this.sanitizeUserData(user)
      })
    } catch (err) {
      errorHandler.handle(err, req, res)
    }
  }

  /**
   * Reading a set of user's data.
   *
   * @method getAll
   * @param {Object} req request object containing:
   *  @prop {Object} query request query parameters containing:
   *    @prop {String} pageNumber pagination offset determinant
   *    @prop {String} recordsPerPage number of rows to be returned
   *    ...and any String type user column key that will serve as a filter
   * @param {Object} res object that will send the response back to its origin
   * @return {null} returns null on success
   */
  async getAll(req, res) {
    try {
      const pageNumber = req.query.pageNumber
        ? req.query.pageNumber
        : 1
      const recordsPerPage = req.query.recordsPerPage
        ? req.query.recordsPerPage
        : 20
      const queryBody = this.queryBuilder.buildQueryBody(
        this.User.modelMappingToArray(),
        req.query
      )

      const users =
        await this.User.getAll(pageNumber, recordsPerPage, queryBody)
      const numberOfPages = Math.ceil(users.count / recordsPerPage)

       res.send({
        success: true,
        numberOfPages,
        data: users
      })

    } catch (err) {
      errorHandler.handle(err, req, res)
    }
  }

  /**
   * Reading a set of user's data from a search query filtered by the query
   *  parameters.
   *
   * @method search
   * @param {Object} req request object containing:
   *  @prop {Object} query request query parameters containing:
   *    @prop {String} searchQuery search query for filtering results
   *    @prop {String} pageNumber pagination offset determinant
   *    @prop {String} recordsPerPage number of rows to be returned
   *    ...and any String type user column key that will serve as a filter
   * @param {Object} res object that will send the response back to its origin
   * @return {null} returns null on success
   */
  async search(req, res) {
    try {
      const searchQuery = req.query.searchQuery
        ? req.query.searchQuery
        : ''
      const pageNumber = req.query.pageNumber
        ? req.query.pageNumber
        : 1
      const recordsPerPage = req.query.recordsPerPage
        ? req.query.recordsPerPage
        : 20

      const builtSearchQuery = this.queryBuilder.buildSearchQuery(
        this.User.modelMappingToArray(),
        searchQuery,
        req.query
      )

      const users =
        await this.User.getAll(
          pageNumber,
          recordsPerPage,
          builtSearchQuery
        )
      const numberOfPages = Math.ceil(users.count / recordsPerPage)

       res.send({
        success: true,
        numberOfPages,
        data: users
      })

    } catch (err) {
      errorHandler.handle(err, req, res)
    }
  }

  /**
   * Updates of user data.
   *
   * @method update
   * @param {Object} req request object containing:
   *  @prop {Object} params URL parameters containing:
   *    @prop {String} id user id
   *  @prop {Object} body JSON body parameters containing update user operation
   *    variables
   * @param {Object} res object that will send the response back to its origin
   * @return {null} returns null on success
   */
  async update(req, res) {
    try {
      const { id } = req.params
      let updatedUser = ''

      this.queryBuilder.validateBody(
        this.User.modelMappingToArray({
          username: false,
          password: true
        }),
        req.body
      )

      if ('password' in req.body) {
        req.body.password = this.cipherManager.encrypt(req.body.password)
      }

      if ('email' in req.body) {
        if (!this.validateEmail(req.body.email)) {
          errorHandler.throw('The email address provided is invalid', 400)
        }
      }

      const user = await this.User.update(id, req.body)

      if (user) {
        updatedUser = await this.User.get({ id })
      } else {
        errorHandler.throw('Failed to update user', 500)
      }

      if (!updatedUser) {
        errorHandler.throw('User does not exist', 404)
      }

      res.send({
        success: true,
        data: this.sanitizeUserData(updatedUser)
      })

    } catch (err) {
      errorHandler.handle(err, req, res)
    }
  }

  /**
   * Deletes user.
   *
   * @method delete
   * @param {Object} req request object containing:
   *  @prop {Object} params URL parameters containing:
   *    @prop {String} id user id
   * @param {Object} res object that will send the response back to its origin
   * @return {null} returns null on success
   */
  async delete(req, res) {
    try {
      const { id } = req.params
      const user = await this.User.delete({ id })

      if (!user) {
        errorHandler.throw('User does not exist', 404)
      }

      res.send({
          success: true,
          message: 'User deleted'
      })
    } catch (err) {
      errorHandler.handle(err, req, res)
    }
  }

  /**
   * Checks database if username is already taken.
   *
   * @method checkExistingUsername
   * @param {String} username username to check
   * @return {null} returns null on success
   */
  async checkExistingUsername(username) {
      const user = await this.User.get({ username })

      if (user) {
        errorHandler.throw('Username is taken', 400)
      }

      return null
  }

  /**
   * Validates if string is a valid email address.
   *
   * @method validateEmail
   * @param {String} email email address to check
   * @return {Function} returns regex validator
   */
  validateEmail(email) {
    // eslint-disable-next-line max-len
    const emailRegexp = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/

    return emailRegexp.test(email)
  }

  /**
   * Removes sensitive data from user details object.
   *
   * @method sanitizeUserData
   * @param {Object} user user details object
   * @return {Object} returns sanitized user data
   */
  sanitizeUserData(user) {
    var sanitizedUser = JSON.parse(JSON.stringify(user))

    Reflect.deleteProperty(sanitizedUser, 'password')

    return sanitizedUser
  }
}
