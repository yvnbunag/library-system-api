/* eslint no-console: off */
import baseController from '@controllers/baseController'
import QueryBuilder from '@lib/sequelize/queryBuilder'
import CipherManager from '@lib/bcrypt/cipherManager'

import fs from 'fs'
import XLSX from 'xlsx'
import generateFilename from 'unique-filename'

import Sequelize from 'sequelize'

/**
 * Controller class for automated user creation/update.
 *
 * @class usersAutomationController
 * @constructor
 */
export default class usersAutomationController extends baseController {
  constructor() {
    super()
    // Binding of class methods(to be used publicly)
    this.export = this.export.bind(this)
    this.import = this.import.bind(this)
    // Initialization of models
    this.User = this.initializeModel('usersModel')
    // Initialization of artifacts
    this.queryBuilder = new QueryBuilder()
    this.userAutomationMapping =
      this.getConfig('export_to_excel.mapping').user_automation
    this.userAutomationMappingInv = {}
    Object.assign(
      this.userAutomationMappingInv,
      ...Object.entries(this.userAutomationMapping).map(([
          index,
          value
        ]) => ({ [value]: index }))
    )
    this.cipherManager = new CipherManager()
  }

  /**
   * Exporting of existing users to an excel file.
   *
   * @method export
   * @param {Object} req request object from origin
   * @param {Object} res object that will send the response back to its origin
   * @return {null} returns null on success
   */
  async export(req, res) {
    try {
      var workbook = XLSX.utils.book_new()
      const fileName = generateFilename('tmp', 'export-users')
      const writeOptions = {
        bookType: 'xlsx',
        bookSST: false
      }
      const header = Object.keys(this.userAutomationMappingInv)
      const users = await this.User.getAllExport()

      users.forEach((user, index) => {
        user.password = this.cipherManager.decrypt(user.password)
        const keyedUser = Object.keys(user).map((key) => {
          const newKey = this.userAutomationMapping[key] || key

          return { [newKey]: user[key] }
        })
        const keyedUserObject = {}

        Object.assign(keyedUserObject, ...keyedUser)
        users[index] = keyedUserObject
      })

      const worksheet = XLSX.utils.json_to_sheet(
        users,
        { header }
      )

      XLSX.utils.book_append_sheet(workbook, worksheet, 'Users')
      XLSX.writeFile(workbook, `${fileName}`, writeOptions)
      this.exportHook(res, fileName)
      res.download(`${fileName}`, 'library-system-users.xlsx')
    } catch (err) {
      errorHandler.handle(err, req, res)
    }
  }

  /**
   * Importing of new users/user updates from an excel file.
   *
   * @method import
   * @param {Object} req request object containing:
   *  @prop {Object} busboy request object that contains uploaded excel file
   * @param {Object} res object that will send the response back to its origin
   * @return {null} returns null on success
   */
  async import(req, res) {
    try {
      var fstream = null
      const fileName = generateFilename('tmp', 'import-users')
      var success = false
      var message = 'Invalid import-file data'

      if (req.busboy !== undefined) {
        req.pipe(req.busboy)

        message = await new Promise((resolve, reject) => {
          try {
            req.busboy.on(
              'file',
              (fieldname, file) => {
                if (fieldname === 'userImport') {
                  console.log('Uploading users import file')
                  fstream = fs.createWriteStream(fileName)
                  file.pipe(fstream)
                  fstream.on(
                    'close',
                    async () => {
                      console.log('Import file upload Finished')
                      this.importHook(res, fileName)
                      try {
                        const users = await this.readImportFile(fileName)
                        const successMessage = 'Users imported successfully'

                        console.log('Import file passed validation')

                        await this.importUsers(users)
                        console.log(successMessage)
                        success = true
                        resolve(successMessage)

                      } catch (err) {
                        reject(err)
                      }
                    }
                  )
                }
              }
            )
          } catch (err) {
            reject(err)
          }
        })
      } else {
        errorHandler.throw('Valid import file attachment missing', 400)
      }

      res.send({
        success,
        message
      })
    } catch (err) {
      errorHandler.handle(err, req, res)
    }
  }

  /**
   * Reads imported users excel file.
   *
   * @method readImportFile
   * @param {String} fileName name of the temporary excel file to be read
   * @return {Array} returns users
   */
  readImportFile(fileName) {
    const workbook = XLSX.readFile(fileName)
    const sheetNameList = workbook.SheetNames
    let users = XLSX.utils.sheet_to_json(workbook.Sheets[sheetNameList[0]])

    users = this.decorateImportObject(users)
    this.validateImportObject(users)

    return users
  }

  /**
   * Validation for each user object read from imported excel file.
   *
   * @method validateImportObject
   * @param {Array} users array of user objects
   * @return {null} returns null on success
   */
  validateImportObject(users) {
    const userMapping = this.User.modelMappingToArray({
      username: true,
      password: true
    })

    if (users.length <= 0) {
      errorHandler.throw('Import file is empty', 400)
    }

    users.forEach((user, index) => {
      Object.keys(userMapping).forEach((key) => {
        const mappedKey = this.userAutomationMapping[key]

        if (!(key in user)) {
          errorHandler.throw(
            `${mappedKey} column is required (missing on row ${index + 2})`
            ,
            400
          )
        }

        if (user[key].length > userMapping[key].length) {
          errorHandler.throw(
            `${mappedKey} column at row ${index + 2} exceeds maximum length ` +
            `(${userMapping[key].length} max, ${user[key].length} given)`
            ,
            400
          )
        }

      })

      if ('email' in user) {
        if (!this.validateEmail(user.email)) {
          errorHandler.throw(
            `Email address provided on row ${index + 2} is invalid`
            ,
            400
          )
        }
      }

    })
  }

  /**
   * Validates if string is a valid email address.
   *
   * @method validateEmail
   * @param {String} email email address to check
   * @return {Function} returns regex validator
   */
  validateEmail(email) {
    // eslint-disable-next-line max-len
    const emailRegexp = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/

    return emailRegexp.test(email)
  }

  /**
   * Decorator for array of user objects for insertion to the database.
   *
   * @method decorateImportObject
   * @param {Array} users array of user objects
   * @return {Array} returns users
   */
  decorateImportObject(users) {
    const userMapping = this.userAutomationMappingInv

    users.forEach((user, index) => {
      const keyedUser = Object.keys(user).map((key) => {
        const newKey = userMapping[key] || key

        return { [newKey]: user[key] }
      })
      const keyedUserObject = {}

      Object.assign(keyedUserObject, ...keyedUser)
      users[index] = keyedUserObject
    })

    return users
  }

  /**
   * Creates/Updates user based on imported user excel file.
   *
   * @method importUsers
   * @param {Array} users array of user objects
   * @return {Object} returns promise object of create/update operations
   */
  importUsers(users) {
    var userImportPromise = []

    users.forEach((user) => {
      user.password = this.cipherManager.encrypt(user.password)
      userImportPromise.push(this.User.createOrUpdateUser(user))
    })

    return Sequelize.Promise.all(userImportPromise)
  }

  /**
   * Creates response post-send hook to delete generated export file.
   *
   * @method exportHook
   * @param {Object} res response object where the hook will be attached
   * @param {String} filename filename of the temporary file to be deleted
   * @return {null} returns null on success
   */
  exportHook(res, filename) {
    res.on('finish', () => {
      fs.stat(filename, (err, stats) => {
        if (err) {
          console.error('Export post-response hook failed (file not generated)')
        }
        if (stats) {
          fs.unlinkSync(filename)
        }
      })
    })
  }

  /**
   * Creates response post-send hook to delete uploaded import file.
   *
   * @method importHook
   * @param {Object} res response object where the hook will be attached
   * @param {String} filename filename of the temporary file to be deleted
   * @return {null} returns null on success
   */
  importHook(res, filename) {
    res.on('finish', () => {
      fs.stat(filename, (err, stats) => {
        if (err) {
          console.error('Import post-response hook failed (file not generated)')
        }
        if (stats) {
          fs.unlinkSync(filename)
        }
      })
    })
  }
}
