/* eslint-disable */
import YmlLoader from '@lib/ymlLoader'

/**
 * Superclass for use of controllers.
 *
 * @class baseController
 */
export default class baseController {
  /**
   * Instantiates models that would be used by child classes.
   *
   * @method initializeModel
   * @param {String} modelName name of the model to be instantiated
   * @return {Object} returns model
   */
  initializeModel(modelName) {
    const model = require(`@models/${modelName}`).default
    
    return new model
  }

  /**
   * Extracts parameter from parameters config file.
   *
   * @method getParameters
   * @param {String} paramName name of the parameter to be extracted
   * @return {String} returns parameter value
   */
  getParameters(paramName) {
    const parameters = new YmlLoader().loadGlobalConfig('parameters')
    return parameters[paramName]
  }

  /**
   * Reads credentials mapping from config file.
   *
   * @method getCredentials
   * @return {Object} returns credentials
   */
  getCredentials() {
    const credentials = new YmlLoader().loadGlobalConfig('credentials.mapping')
    return credentials
  }

  /**
   * Reads items mapping from config file.
   *
   * @method getItemsMapping
   * @return {Object} returns itemsMapping
   */
  getItemsMapping() {
    const itemsMapping = new YmlLoader().loadGlobalConfig('items.mapping')
    return itemsMapping
  }

  /**
   * Reads borrow settings from config file.
   *
   * @method getBorrowSettings
   * @return {Object} returns borrowSettings
   */
  getBorrowSettings() {
    const borrowSettings = new YmlLoader().loadGlobalConfig('borrow.settings')
    return borrowSettings
  }

  /**
   * Reads configuration variables from config file.
   *
   * @method getConfig
   * @param {String} configName name of the config file to read
   * @return {Object} returns config
   */
  getConfig(configName) {
    const config = new YmlLoader().loadGlobalConfig(configName)
    return config
  }
}
