/* eslint max-statements: ["error", 15] */
import baseController from '@controllers/baseController'
import jwt from 'jsonwebtoken'
import CipherManager from '@lib/bcrypt/cipherManager'

/**
 * Controller class for user authentication.
 *
 * @class userAuthenticationController
 * @constructor
 */
export default class userAuthenticationController extends baseController {
  constructor() {
    super()
    // Binding of class methods(to be used publicly)
    this.index = this.index.bind(this)
    this.cipherManager = new CipherManager()
  }

  /**
   * Validation of user credentials and generation of access token.
   *
   * @method index
   * @param {Object} req request object containing:
   *  @prop {Object} body JSON body parameters containing:
   *    @prop {String} username
   *    @prop {String} password
   * @param {Object} res object that will send the response back to its origin
   * @return {null} returns null on success
   */
  async index(req, res) {
      try {
        const User = this.initializeModel('usersModel')
        var { username, password } = req.body

        password = this.cipherManager.encrypt(password)

        const user = await User.validate(username, password)

        if (!user) {
          errorHandler.throw('unauthorized', 401)
        }

        if (!this.validateStatusCredential(user.status)) {
          errorHandler.throw('not allowed', 403)
        }

        const token = jwt.sign(
          {
            id: user.id,
            username: user.username,
            password: user.password
          },
          process.env.JWT_PASSPHRASE,
          {
            // 86400 Expires in 24 hours
            expiresIn: 86400
          }
        )

        res.send({
          success: true,
          bearerToken: token
        })
      } catch (err) {
        errorHandler.handle(err, req, res)
      }
  }

  /**
   * Checks validity of user status.
   *
   * @method validateStatusCredential
   * @param {String} status user status to be validated
   * @return {Boolean} returns statusCredential
   */
  validateStatusCredential(status) {
    const credentials = this.getCredentials()
    let statusCredential = 'Invalid'

    credentials.status.allowed.forEach((value) => {
      if (value === status) {
        statusCredential = true
      }
    })

    credentials.status.prohibited.forEach((value) => {
      if (value === status) {
        statusCredential = false
      }
    })

    if (typeof statusCredential !== 'boolean') {
      errorHandler.throw('Invalid user status', 500)
    }

    return statusCredential
  }
}
