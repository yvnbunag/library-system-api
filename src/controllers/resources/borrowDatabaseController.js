/* eslint max-statements: ["error", 25] */
import baseController from '@controllers/baseController'
import QueryBuilder from '@lib/sequelize/queryBuilder'

/**
 * Controller class for user borrow requests.
 *
 * @class borrowDatabaseController
 * @constructor
 */
export default class borrowDatabaseController extends baseController {
  constructor() {
    super()
    // Binding of class methods(to be used publicly)
    this.createBorrowRequest = this.createBorrowRequest.bind(this)
    this.getBorrowRequests = this.getBorrowRequests.bind(this)
    this.getBorrowHistory = this.getBorrowHistory.bind(this)
    this.cancelBorrowRequest = this.cancelBorrowRequest.bind(this)
    // Initialization of models
    this.Borrow = this.initializeModel('borrowModel')
    this.Resource = this.initializeModel('resourcesModel')
    // Initialization of artifacts
    this.queryBuilder = new QueryBuilder()
  }

  /**
   * Creates borrow request of a user for a document.
   *
   * @method createBorrowRequest
   * @param {Object} req request object containing:
   *  @prop {Object} params URL parameters containing:
   *    @prop {String} user_id
   *    @prop {String} resource_id
   * @param {Object} res object that will send the response back to its origin
   * @return {null} returns null on success
   */
  async createBorrowRequest(req, res) {
    try {
      const { user_id, resource_id } = req.params

      this.validateUserIdOwnership(req.user.id, user_id)
      await this.validateUserBorrowQueue(user_id)
      await this.checkResourceAvailability(resource_id)

      const borrowVariables = {
        user_id,
        resource_id,
        status: 'pending'
      }
      const borrow = await this.Borrow.create(borrowVariables)

      if (!borrow) {
        errorHandler.throw('Failed to borrow resource', 500)
      }

      res.send({
        success: true,
        data: borrow
      })
    } catch (err) {
      errorHandler.handle(err, req, res)
    }
  }

  /**
   * Reads user borrow request.
   *
   * @method getBorrowRequests
   * @param {Object} req request object containing:
   *  @prop {Object} params URL parameters containing:
   *    @prop {String} user_id
   * @param {Object} res object that will send the response back to its origin
   * @return {null} returns null on success
   */
  async getBorrowRequests(req, res) {
    try {
      const { user_id } = req.params
      const pageNumber = 1
      const recordsPerPage = 2

      this.validateUserIdOwnership(req.user.id, user_id)

      req.query.user_id = user_id
      const queryBody = this.queryBuilder.buildQueryBody(
        this.Borrow.modelMappingToArray(),
        req.query
      )

      this.Borrow.associateRelatedTables()
      const include = [this.Resource.getInclusionAttributes()]

      var borrows = {}

      borrows.count = await this.Borrow.count(
        queryBody,
        include
      )
      borrows.rows = await this.Borrow.getAllJoined(
        pageNumber,
        recordsPerPage,
        queryBody,
        include
      )
      const numberOfPages = Math.ceil(borrows.count / recordsPerPage)

      res.send({
        success: true,
        numberOfPages,
        data: this.decorateJoinedBorrowRows(borrows)
      })
    } catch (err) {
      errorHandler.handle(err, req, res)
    }
  }

  /**
   * Reads user borrow history.
   *
   * @method getBorrowHistory
   * @param {Object} req request object containing:
   *  @prop {Object} params URL parameters containing:
   *    @prop {String} user_id
   *  @prop {Object} query request query parameters containing:
   *    @prop {String} pageNumber pagination offset determinant
   *    @prop {String} recordsPerPage number of rows to be returned
   * @param {Object} res object that will send the response back to its origin
   * @return {null} returns null on success
   */
  async getBorrowHistory(req, res) {
    try {
      const { user_id } = req.params
      const pageNumber = req.query.pageNumber
          ? req.query.pageNumber
          : 1
      const recordsPerPage = req.query.recordsPerPage
          ? req.query.recordsPerPage
          : 20

      this.validateUserIdOwnership(req.user.id, user_id)

      const queryBody = this.queryBuilder.buildBorrowOrQuery(
        user_id,
        [
          {
            status: 'rejected'
          },
          {
            status: 'returned'
          }
        ]
      )

      this.Borrow.associateRelatedTables()
      const include = [this.Resource.getInclusionAttributes()]

      var borrows = {}

      borrows.count = await this.Borrow.count(
        queryBody,
        include
      )
      borrows.rows = await this.Borrow.getAllJoined(
        pageNumber,
        recordsPerPage,
        queryBody,
        include
      )
      const numberOfPages = Math.ceil(borrows.count / recordsPerPage)

      res.send({
        success: true,
        numberOfPages,
        data: this.decorateJoinedBorrowRows(borrows)
      })

    } catch (err) {
      errorHandler.handle(err, req, res)
    }
  }

  /**
   * Deletes borrow request that the user cancels.
   *
   * @method cancelBorrowRequest
   * @param {Object} req request object containing:
   *  @prop {Object} params URL parameters containing:
   *    @prop {String} user_id
   *    @prop {String} borrow_id
   * @param {Object} res object that will send the response back to its origin
   * @return {null} returns null on success
   */
  async cancelBorrowRequest(req, res) {
    try {
      const { user_id, borrow_id } = req.params

      this.validateUserIdOwnership(req.user.id, user_id)
      await this.checkBorrowPending(borrow_id)

      const borrow = await this.Borrow.delete({ id: borrow_id })

      if (!borrow) {
        errorHandler.throw('Failed to cancel request', 404)
      }

      res.send({
          success: true,
          message: 'Borrow request cancelled'
      })
    } catch (err) {
      errorHandler.handle(err, req, res)
    }
  }

  /**
   * Compares stored user id from request user id
   *
   * @method validateUserIdOwnership
   * @param {String} id user id from JWT
   * @param {String} request_id user id from URL parameter
   * @return {null} returns null on success
   */
  validateUserIdOwnership(id, request_id) {
    if (parseInt(id, 10) !== parseInt(request_id, 10)) {
      errorHandler.throw('You do not own the account id', 403)
    }
  }

  /**
   * Validates if the user borrow queue count has reached its limit.
   *
   * @method validateUserBorrowQueue
   * @param {string} user_id id for reading current user borrow queue count
   * @return {null} returns null on success
   */
  async validateUserBorrowQueue(user_id) {
    const checkCountQuery = this.queryBuilder.buildBorrowOrQuery(
        user_id,
        [
          {
            status: 'pending'
          },
          {
            status: 'lent'
          }
        ]
      )
      const borrows = await this.Borrow.getAll(
        1,
        10,
        checkCountQuery
      )

      if (borrows.count > 1) {
        errorHandler.throw('Borrow queue limit reached', 400)
      }
  }

  /**
   * Validates resource availability.
   *
   * @method checkResourceAvailability
   * @param {string} id resource id
   * @return {null} returns null on success
   */
  async checkResourceAvailability(id) {
      const resourceAvailability = await this.Resource.get({ id })

      if (!resourceAvailability) {
        errorHandler.throw('Resource does not exist', 400)
      }
      if (resourceAvailability.borrowed >= resourceAvailability.count) {
        errorHandler.throw('Resource not available', 403)
      }
  }

  /**
   * Validates if pending borrow request exists.
   *
   * @method checkBorrowPending
   * @param {string} id resource id for checking if borrow request exists
   * @return {null} returns null on success
   */
  async checkBorrowPending(id) {
      const checkResourceStatus = await this.Borrow.get({ id })

      if (!checkResourceStatus) {
        errorHandler.throw('Borrow request does not exist', 400)
      }
      if (checkResourceStatus.status !== 'pending') {
        errorHandler.throw('Cannot cancel non pending borrow request', 403)
      }
  }

  /**
   * Transforms resource details from string to JSON.
   *
   * @method decorateJoinedBorrowRow
   * @param {Object} body borrow request body
   * @return {Object} returns body
   */
  decorateJoinedBorrowRow(body) {
    body.resource.details = JSON.parse(body.resource.details)

    return body
  }

  /**
   * Transforms all resource details on an array of borrows from string to JSON.
   *
   * @method decorateJoinedBorrowRows
   * @param {Array} borrows array of borrow requests
   * @return {Array} returns borrows
   */
  decorateJoinedBorrowRows(borrows) {
    Object.keys(borrows.rows).forEach((key) => {
        borrows.rows[key] = this.decorateJoinedBorrowRow(borrows.rows[key])
    })

    return borrows
  }
}
