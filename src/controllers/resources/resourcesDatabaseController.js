/* eslint max-statements: ["error", 35] */
/* eslint max-lines-per-function: off */
/* eslint no-console: off */
/* eslint function-paren-newline: off */

import baseController from '@controllers/baseController'
import QueryBuilder from '@lib/sequelize/queryBuilder'
import Sequelize from 'sequelize'

import fs from 'fs'
import XLSX from 'xlsx'
import generateFilename from 'unique-filename'
import moment from 'moment'

/**
 * Controller class for resources management.
 *
 * @class resourcesDatabaseController
 * @constructor
 */
export default class resourcesDatabaseController extends baseController {
  constructor() {
    super()
    // Binding of class methods(to be used publicly)
    this.create = this.create.bind(this)
    this.get = this.get.bind(this)
    this.getAll = this.getAll.bind(this)
    this.search = this.search.bind(this)
    this.update = this.update.bind(this)
    this.delete = this.delete.bind(this)
    this.generateReport = this.generateReport.bind(this)
    // Initialization of models
    this.Borrow = this.initializeModel('borrowModel')
    this.Resource = this.initializeModel('resourcesModel')
    // Initialization of artifacts
    this.queryBuilder = new QueryBuilder()
    this.generateReportMapping =
      this.getConfig('export_to_excel.mapping').resource_generate_report
    this.generateReportMappingInv = {}
    Object.assign(
      this.generateReportMappingInv,
      ...Object.entries(this.generateReportMapping).map(([
          index,
          value
        ]) => ({ [value]: index }))
    )
  }

  /**
   * Creates of document/digital resources.
   *
   * @method create
   * @param {Object} req request object containing:
   *  @prop {Object} body JSON body parameters containing create resource
   *     operation variables
   * @param {Object} res object that will send the response back to its origin
   * @return {null} returns null on success
   */
  async create(req, res) {
    try {
      const body = this.formatResource(req.body)

      this.queryBuilder.validateBody(
        this.Resource.modelMappingToArray(),
        body,
        {
          requireAll: true
        }
      )

      const resource = await this.Resource.create(body)

      if (!resource) {
        errorHandler.throw('Failed to create resource', 500)
      }

      res.send({
        success: true,
        data: this.decorateResource(resource)
      })
    } catch (err) {
      errorHandler.handle(err, req, res)
    }
  }

  /**
   * Reads individual resource details.
   *
   * @method get
   * @param {Object} req request object containing:
   *  @prop {Object} params URL parameters containing:
   *    @prop {String} id resource id
   * @param {Object} res object that will send the response back to its origin
   * @return {null} returns null on success
   */
  async get(req, res) {
    try {
      const { id } = req.params

      this.Resource.associateRelatedTables()
      const include = [this.Borrow.getInclusionAttributes()]

      const resource = await this.Resource.getJoined({ id }, include)

      if (!resource) {
        errorHandler.throw('Resource does not exist', 404)
      }

      res.send({
        success: true,
        data: this.decorateResource(resource)
      })
    } catch (err) {
      errorHandler.handle(err, req, res)
    }
  }

  /**
   * Reads resources filtered by the query parameters.
   *
   * @method getAll
   * @param {Object} req request object containing:
   *  @prop {Object} query request query parameters containing:
   *    @prop {String} pageNumber pagination offset determinant
   *    @prop {String} recordsPerPage number of rows to be returned
   *    @prop {Array} includedTypes resource types to be included
   *    @prop {Boolean} availabilityRequired determines if resource is required
   *      to be available
   * @param {Object} res object that will send the response back to its origin
   * @return {null} returns null on success
   */
  async getAll(req, res) {
    try {
      const pageNumber = req.query.pageNumber
        ? req.query.pageNumber
        : 1
      const recordsPerPage = req.query.recordsPerPage
        ? req.query.recordsPerPage
        : 20
      const { availabilityRequired } = req.query
      let excludeNoCount = true

      const queryBody = this.queryBuilder.buildQueryBody(
        this.Resource.modelMappingToArray(),
        req.query
      )

      this.Resource.associateRelatedTables()
      const include = [this.Borrow.getInclusionAttributes()]

      let filteredBody = this.queryBuilder.buildQueryFilters(
        queryBody,
        req.query.includedTypes,
        this.getItemsMapping()
      )

      if (availabilityRequired !== undefined && availabilityRequired) {
        if (availabilityRequired === 'true' ||
          availabilityRequired === 'false') {
          filteredBody = this.queryBuilder.requireAvailability(
            JSON.parse(availabilityRequired),
            filteredBody
          )
        } else if (availabilityRequired === 'showWithdrawn') {
          excludeNoCount = false
        }
      }

      if (excludeNoCount) {
        filteredBody = this.queryBuilder
          .buildQueryExcludeNoCount(filteredBody)
      } else {
        filteredBody = this.queryBuilder
          .buildQueryExcludeWithCount(filteredBody)
      }

      const resources =
        await this.Resource.getAllJoined(
          pageNumber,
          recordsPerPage,
          filteredBody,
          include
        )
      const numberOfPages = Math.ceil(resources.count / recordsPerPage)

      res.send({
        success: true,
        numberOfPages,
        data: this.decorateResources(resources)
      })

    } catch (err) {
      errorHandler.handle(err, req, res)
    }
  }

  /**
   * Reads resources from a search query filtered by the query parameters.
   *
   * @method search
   * @param {Object} req request object containing:
   *  @prop {Object} query request query parameters containing:
   *    @prop {String} searchQuery search query for filtering results
   *    @prop {String} pageNumber pagination offset determinant
   *    @prop {String} recordsPerPage number of rows to be returned
   *    @prop {Array} includedTypes resource types to be included
   *    @prop {Boolean} availabilityRequired determines if resource is required
   *      to be available
   * @param {Object} res object that will send the response back to its origin
   * @return {null} returns null on success
   */
  async search(req, res) {
    try {
      const searchQuery = req.query.searchQuery
        ? req.query.searchQuery
        : ''
      const pageNumber = req.query.pageNumber
        ? req.query.pageNumber
        : 1
      const recordsPerPage = req.query.recordsPerPage
        ? req.query.recordsPerPage
        : 20
      const { availabilityRequired } = req.query
      let excludeNoCount = true

      const builtSearchQuery = this.queryBuilder.buildSearchQuery(
        this.Resource.modelMappingToArray(),
        searchQuery,
        req.query
      )

      let filteredBody = this.queryBuilder.buildQueryFilters(
        builtSearchQuery,
        req.query.includedTypes,
        this.getItemsMapping()
      )

      if (availabilityRequired !== undefined && availabilityRequired) {
        if (availabilityRequired === 'true' ||
          availabilityRequired === 'false') {
          filteredBody = this.queryBuilder.requireAvailability(
            JSON.parse(availabilityRequired),
            filteredBody
          )
        } else if (availabilityRequired === 'showWithdrawn') {
          excludeNoCount = false
        }
      }

      if (excludeNoCount) {
        filteredBody = this.queryBuilder
          .buildQueryExcludeNoCount(filteredBody)
      } else {
        filteredBody = this.queryBuilder
          .buildQueryExcludeWithCount(filteredBody)
      }

      this.Resource.associateRelatedTables()
      const include = [this.Borrow.getInclusionAttributes()]

      const resources =
        await this.Resource.getAllJoined(
          pageNumber,
          recordsPerPage,
          filteredBody,
          include
        )
      const numberOfPages = Math.ceil(resources.count / recordsPerPage)

       res.send({
        success: true,
        numberOfPages,
        data: this.decorateResources(resources)
      })

    } catch (err) {
      errorHandler.handle(err, req, res)
    }
  }

  /**
   * Updates of resource column values.
   *
   * @method update
   * @param {Object} req request object containing:
   *  @prop {Object} params URL parameters containing:
   *    @prop {String} id resource id
   *  @prop {Object} body JSON body parameters containing update resource
   *     operation variables
   * @param {Object} res object that will send the response back to its origin
   * @return {null} returns null on success
   */
  async update(req, res) {
    try {
      const { id } = req.params

      this.Resource.associateRelatedTables()
      const include = [this.Borrow.getInclusionAttributes()]

      let resourceChecker = await this.Resource.getJoined({ id }, include)

      if (!resourceChecker) {
        errorHandler.throw('Resource does not exist', 404)
      }

      resourceChecker = this.decorateResource(resourceChecker)

      if (req.body.classification) {
        if (
          req.body.classification !== resourceChecker.classification &&
          resourceChecker.hasHistory
        ) {
          const errorMessage =
            'Resource cannot be updated to a different classification ' +
            'as it has reference to a borrow request'

          errorHandler.throw(
            errorMessage,
            403
          )
        }
      }

      let updatedResource = ''

      const body = this.formatResource(req.body)

      this.queryBuilder.validateBody(
        this.Resource.modelMappingToArray(),
        body
      )

      const resource = await this.Resource.update(
        id,
        body
      )

      if (resource) {
        updatedResource = await this.Resource.getJoined({ id }, include)
      } else {
        errorHandler.throw('Failed to update resource', 500)
      }

      if (!updatedResource) {
        errorHandler.throw('Resource does not exist', 404)
      }

      res.send({
        success: true,
        data: this.decorateResource(updatedResource)
      })

    } catch (err) {
      errorHandler.handle(err, req, res)
    }
  }

  /**
   * Deletes resource.
   *
   * @method delete
   * @param {Object} req request object containing:
   *  @prop {Object} params URL parameters containing:
   *    @prop {String} id resource id
   * @param {Object} res object that will send the response back to its origin
   * @return {null} returns null on success
   */
  async delete(req, res) {
    try {
      const { id } = req.params

      this.Resource.associateRelatedTables()
      const include = [this.Borrow.getInclusionAttributes()]

      let resourceChecker = await this.Resource.getJoined({ id }, include)

      if (!resourceChecker) {
        errorHandler.throw('Resource does not exist', 404)
      }

      resourceChecker = this.decorateResource(resourceChecker)

      if (resourceChecker.hasHistory) {
        const errorMessage =
          'Resource cannot be deleted ' +
          'as it has reference to a borrow request'

        errorHandler.throw(
          errorMessage,
          403
        )
      }

      await this.Resource.delete({ id })

      res.send({
          success: true,
          message: 'Resource deleted'
      })
    } catch (err) {
      errorHandler.handle(err, req, res)
    }
  }

  /**
   * Generation of resources report from a search query including all rows.
   *
   * @method generateReport
   * @param {Object} req request object containing:
   *  @prop {Object} query request query parameters containing:
   *    @prop {String} searchQuery search query for filtering results
   *    @prop {Array} includedTypes resource types to be included
   *    @prop {Boolean} availabilityRequired determines if resource is required
   *      to be available
   * @param {Object} res object that will send the response back to its origin
   * @return {null} returns null on success
   */
  async generateReport(req, res) {
    try {
      const searchQuery = req.query.searchQuery
        ? req.query.searchQuery
        : ''

      const { availabilityRequired } = req.query
      let excludeNoCount = true

      const builtSearchQuery = this.queryBuilder.buildSearchQuery(
        this.Resource.modelMappingToArray(),
        searchQuery,
        req.query
      )

      let filteredBody = this.queryBuilder.buildQueryFilters(
        builtSearchQuery,
        req.query.includedTypes,
        this.getItemsMapping()
      )

      if (availabilityRequired !== undefined && availabilityRequired) {
        if (availabilityRequired === 'true' ||
          availabilityRequired === 'false') {
          filteredBody = this.queryBuilder.requireAvailability(
            JSON.parse(availabilityRequired),
            filteredBody
          )
        } else if (availabilityRequired === 'showWithdrawn') {
          excludeNoCount = false
        }
      }

      if (excludeNoCount) {
        filteredBody = this.queryBuilder
          .buildQueryExcludeNoCount(filteredBody)
      } else {
        filteredBody = this.queryBuilder
          .buildQueryExcludeWithCount(filteredBody)
      }

      this.Resource.associateRelatedTables()

      const attributes = {
        include: [
          [
            Sequelize.fn('SUM', Sequelize.where(
              Sequelize.col('borrows.status'),
              'pending'
            )),
            'total_pending'
          ],
          [
            Sequelize.fn('SUM', Sequelize.where(
              Sequelize.col('borrows.status'),
              'lent'
            )),
            'total_lent'
          ],
          [
            Sequelize.fn('SUM', Sequelize.where(
              Sequelize.col('borrows.status'),
              'returned'
            )),
            'total_returned'
          ],
          [
            Sequelize.fn('SUM', Sequelize.where(
              Sequelize.col('borrows.status'),
              'rejected'
            )),
            'total_rejected'
          ]
        ]
      }
      const include = [this.Borrow.getGenerateReportInclusionAttributes()]
      const group = ['id']
      const resources =
        await this.Resource.generateReport(
          attributes,
          filteredBody,
          include,
          group
        )

      var workbook = XLSX.utils.book_new()
      const fileName = generateFilename('tmp', 'generate-report-resources')
      const writeOptions = {
        bookType: 'xlsx',
        bookSST: false
      }

      const jsonResources = this.decorateResourceGenerateReport(resources)

      const header = Object.keys(this.generateReportMappingInv)

      const generatedDate = moment()
        .utcOffset(480)
        .format('LLL')
        .toString()
      var worksheet = XLSX.utils.aoa_to_sheet(
        [
          [
            'Date generated: ',
            generatedDate
          ],
          [
            'Generated by: ',
            `${req.user.first_name} ` +
            `${req.user.middle_initial.charAt(0)}. ` +
            `${req.user.last_name}`
          ],
          [
            'Number of Rows:',
            resources.length
          ]
        ]
      )

      XLSX.utils.sheet_add_json(
        worksheet,
        jsonResources,
        {
          header,
          origin: 'A5'
        }
      )

      const date = moment()
        .utcOffset(480)
        .format('MMM-DD-YYYY--hh-mma')
        .toString()
      const downloadFileName = `resources-report-${date}.xlsx`

      XLSX.utils.book_append_sheet(workbook, worksheet, 'Resources')
      XLSX.writeFile(workbook, `${fileName}`, writeOptions)
      this.generateHook(res, fileName)
      res.header('Access-Control-Expose-Headers', 'Content-Disposition')
      res.download(`${fileName}`, downloadFileName)
    } catch (err) {
      errorHandler.handle(err, req, res)
    }
  }

  /**
   * Decorates an array of resource objects for generated report.
   *
   * @method decorateResourceGenerateReport
   * @param {Array} resources collection of resource objects
   * @return {Array} returns resourcesShadow
   */
  decorateResourceGenerateReport(resources) {
    let resourcesShadow = resources

    resourcesShadow = JSON.parse(JSON.stringify(resources))

    resourcesShadow.forEach((resource, index) => {
      let decoratedDetails = ''
      const details = JSON.parse(resource.details)
      const lastKey = Object.keys(details)[Object.keys(details).length - 1]

      Object.keys(details).forEach((key) => {
        decoratedDetails += `${this.jsUcFirst(key)}: ${details[key]}`
        if (key !== lastKey) {
          decoratedDetails += ', '
        }
      })

      if (resource.classification === 'document') {
        resource.total_pending = resource.total_pending === null
          ? 0
          : parseInt(resource.total_pending, 10)
        resource.total_lent = resource.total_lent === null
          ? 0
          : parseInt(resource.total_lent, 10)
        resource.total_returned = resource.total_returned === null
          ? 0
          : parseInt(resource.total_returned, 10)
        resource.total_rejected = resource.total_rejected === null
          ? 0
          : parseInt(resource.total_rejected, 10)

          resource.available_copies =
            resource.count - resource.borrowed
      } else {
        resource.total_pending = 'n/a'
        resource.total_lent = 'n/a'
        resource.total_returned = 'n/a'
        resource.total_rejected = 'n/a'
        resource.available_copies = 'n/a'
        resource.borrowed = 'n/a'
        resource.count = 'n/a'
      }

      resource.details = decoratedDetails
      resource.classification = this.jsUcFirst(resource.classification)
      resource.type = this.jsUcFirst(resource.type)

      if ('id' in resourcesShadow[index]) {
        Reflect.deleteProperty(resourcesShadow[index], 'id')
      }

      if ('borrows' in resourcesShadow[index]) {
        Reflect.deleteProperty(resourcesShadow[index], 'borrows')
      }

      if ('total_lent' in resourcesShadow[index]) {
        Reflect.deleteProperty(resourcesShadow[index], 'total_lent')
      }

      const keyedResource = Object.keys(resource).map((key) => {
        const newKey = this.generateReportMapping[key] || key

        return { [newKey]: resource[key] }
      })
      const keyedResourceObject = {}

        Object.assign(keyedResourceObject, ...keyedResource)
        resourcesShadow[index] = keyedResourceObject
    })

    return resourcesShadow
  }

  /**
   * Transforms first letter of a string to uppercase format.
   *
   * @method jsUcFirst
   * @param {String} string variable to be transformed
   * @return {String} returns string
   */
  jsUcFirst(string) {
    if (string !== '' && string !== undefined && string !== null) {
      return string.charAt(0).toUpperCase() + string.slice(1)
    }

    return null
  }

  /**
   * Creates response post-send hook to delete generated report file.
   *
   * @method generateHook
   * @param {Object} res response object where the hook will be attached
   * @param {String} filename filename of the temporary file to be deleted
   * @return {null} returns null on success
   */
  generateHook(res, filename) {
    res.on('finish', () => {
      fs.stat(filename, (err, stats) => {
        if (err) {
          console.error(
            'Generate report post-response hook failed (file not generated)'
          )
        }
        if (stats) {
          fs.unlinkSync(filename)
        }
      })
    })
  }

  /**
   * Transforms details property of a resource from string to JSON.
   *
   * @method formatResource
   * @param {Object} body resource object body
   * @return {Object} returns body
   */
  formatResource(body) {
    body.details = JSON.stringify(body.details)

    return body
  }

  /**
   * Decorates resource object.
   *
   * @method decorateResource
   * @param {Object} body resource object body
   * @return {Object} returns bodyShadow
   */
  decorateResource(body) {
    const bodyShadow = JSON.parse(JSON.stringify(body))

    bodyShadow.details = JSON.parse(bodyShadow.details)

    if ('borrows' in bodyShadow) {
      if (bodyShadow.borrows.length) {
        bodyShadow.hasHistory = true
      } else {
        bodyShadow.hasHistory = false
      }

      Reflect.deleteProperty(bodyShadow, 'borrows')
    }

    return bodyShadow
  }

  /**
   * Decorates an array of resource objects.
   *
   * @method decorateResources
   * @param {Array} resources collection of resource objects
   * @return {Array} returns resources
   */
  decorateResources(resources) {
    Object.keys(resources.rows).forEach((key) => {
        resources.rows[key] = this.decorateResource(resources.rows[key])
    })

    return resources
  }
}
