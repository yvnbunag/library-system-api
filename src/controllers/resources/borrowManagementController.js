/* eslint
    max-statements: ["error", 30]
    max-lines-per-function:  ["error", 120]
*/
import baseController from '@controllers/baseController'
import QueryBuilder from '@lib/sequelize/queryBuilder'

/**
 * Controller class for managing borrows requests.
 *
 * @class borrowManagementController
 * @constructor
 */
export default class borrowManagementController extends baseController {
  constructor() {
    super()
    // Binding of class methods(to be used publicly)
    this.getPending = this.getPending.bind(this)
    this.searchPending = this.searchPending.bind(this)
    this.getLent = this.getLent.bind(this)
    this.searchLent = this.searchLent.bind(this)
    this.getHistory = this.getHistory.bind(this)
    this.searchHistory = this.searchHistory.bind(this)
    this.updateBorrow = this.updateBorrow.bind(this)
    // Initialization of models
    this.User = this.initializeModel('usersModel')
    this.Borrow = this.initializeModel('borrowModel')
    this.Resource = this.initializeModel('resourcesModel')
    // Initialization of artifacts
    this.queryBuilder = new QueryBuilder()
    this.borrowSettings = this.getBorrowSettings()
  }

  /**
   * Reads pending borrow requests filtered by the query parameters.
   *
   * @method getPending
   * @param {Object} req request object containing:
   *  @prop {Object} query request query parameters containing:
   *    @prop {String} pageNumber pagination offset determinant
   *    @prop {String} recordsPerPage number of rows to be returned
   *    @prop {Boolean} resourceAvailability determines if resource is required
   *      to be available
   * @param {Object} res object that will send the response back to its origin
   * @return {null} returns null on success
   */
  async getPending(req, res) {
    try {
      const pageNumber = req.query.pageNumber
          ? req.query.pageNumber
          : 1
      const recordsPerPage = req.query.recordsPerPage
          ? req.query.recordsPerPage
          : 20
      const resourceAvailability = req.query.resourceAvailability
          ? req.query.resourceAvailability
          : false

      let queryBody = this.queryBuilder.buildQueryBody(
        this.Borrow.modelMappingToArray({
          user_id: false,
          resource_id: false
        }),
        { status: 'pending' }
      )

      this.Borrow.associateRelatedTables()
      const include = [
        this.User.getInclusionAttributes(),
        this.Resource.getInclusionAttributes()
      ]

      if (resourceAvailability) {
        queryBody = this.queryBuilder.requestResourceAvailability(
          resourceAvailability,
          queryBody
        )
      }

      var borrows = {}

      borrows.count = await this.Borrow.count(
        queryBody,
        include
      )
      borrows.rows = await this.Borrow.getAllJoined(
        pageNumber,
        recordsPerPage,
        queryBody,
        include
      )
      const numberOfPages = Math.ceil(borrows.count / recordsPerPage)

      res.send({
        success: true,
        numberOfPages,
        data: this.decorateJoinedBorrowRows(borrows)
      })
    } catch (err) {
      errorHandler.handle(err, req, res)
    }
  }

  /**
   * Reads pending borrow requests from a search query filtered by the
   *  query parameters.
   *
   * @method searchPending
   * @param {Object} req request object containing the following:
   *  @prop {Object} query request query parameters containing:
   *    @prop {String} searchQuery search query for filtering results
   *    @prop {String} pageNumber pagination offset determinant
   *    @prop {String} recordsPerPage number of rows to be returned
   *    @prop {Boolean} resourceAvailability determines if resource is required
   *      to be available
   * @param {Object} res object that will send the response back to its origin
   * @return {null} returns null on success
   */
  async searchPending(req, res) {
    try {
      const searchQuery = req.query.searchQuery
        ? req.query.searchQuery
        : ''
      const pageNumber = req.query.pageNumber
        ? req.query.pageNumber
        : 1
      const recordsPerPage = req.query.recordsPerPage
        ? req.query.recordsPerPage
        : 20
      const resourceAvailability = req.query.resourceAvailability
          ? req.query.resourceAvailability
          : false

      var modelMappingArray = this.Borrow.modelMappingToArray({
        user_id: false,
        resource_id: false
      })
      const userInclusion = this.User.getInclusionAttributes()
      const resourceInclusion = this.Resource.getInclusionAttributes()

      this.Borrow.associateRelatedTables()
      const include = [
        userInclusion,
        resourceInclusion
      ]

      modelMappingArray = this.queryBuilder.pushAssociatedModelMappingForJoined(
        modelMappingArray,
        [
          userInclusion,
          resourceInclusion
        ]
      )

      let queryBody = this.queryBuilder.buildSearchQuery(
        modelMappingArray,
        searchQuery,
        { status: 'pending' }
      )

      if (resourceAvailability) {
        queryBody = this.queryBuilder.requestResourceAvailability(
          resourceAvailability,
          queryBody
        )
      }

      var borrows = {}

      borrows.count = await this.Borrow.count(
        queryBody,
        include
      )
      borrows.rows = await this.Borrow.getAllJoined(
        pageNumber,
        recordsPerPage,
        queryBody,
        include
      )
      const numberOfPages = Math.ceil(borrows.count / recordsPerPage)

      res.send({
        success: true,
        numberOfPages,
        data: this.decorateJoinedBorrowRows(borrows)
      })
    } catch (err) {
      errorHandler.handle(err, req, res)
    }
  }

  /**
   * Reads active borrow requests filtered by the query parameters.
   *
   * @method getLent
   * @param {Object} req request object containing:
   *  @prop {Object} query request query parameters containing:
   *    @prop {String} pageNumber pagination offset determinant
   *    @prop {String} recordsPerPage number of rows to be returned
   *    @prop {String} borrowStatus filter for determining if active borrow
   *      request should be on-going or overdue
   * @param {Object} res object that will send the response back to its origin
   * @return {null} returns null on success
   */
  async getLent(req, res) {
    try {
      const pageNumber = req.query.pageNumber
          ? req.query.pageNumber
          : 1
      const recordsPerPage = req.query.recordsPerPage
          ? req.query.recordsPerPage
          : 20
      const borrowStatus = req.query.borrowStatus
          ? req.query.borrowStatus
          : false

      let queryBody = this.queryBuilder.buildQueryBody(
        this.Borrow.modelMappingToArray({
          user_id: false,
          resource_id: false
        }),
        { status: 'lent' }
      )

      if (borrowStatus) {
        queryBody = this.queryBuilder.borrowStatusLent(
          borrowStatus,
          queryBody
        )
      }

      this.Borrow.associateRelatedTables()
      const include = [
        this.User.getInclusionAttributes(),
        this.Resource.getInclusionAttributes()
      ]

      var borrows = {}

      borrows.count = await this.Borrow.count(
        queryBody,
        include
      )
      borrows.rows = await this.Borrow.getAllJoined(
        pageNumber,
        recordsPerPage,
        queryBody,
        include
      )
      const numberOfPages = Math.ceil(borrows.count / recordsPerPage)

      res.send({
        success: true,
        numberOfPages,
        data: this.decorateJoinedBorrowRows(borrows)
      })
    } catch (err) {
      errorHandler.handle(err, req, res)
    }
  }

  /**
   * Reads active borrow requests from a search query filtered by the query
   *  parameters.
   *
   * @method searchLent
   * @param {Object} req request object containing:
   *  @prop {Object} query request query parameters containing:
   *    @prop {String} searchQuery search query for filtering results
   *    @prop {String} pageNumber pagination offset determinant
   *    @prop {String} recordsPerPage number of rows to be returned
   *    @prop {String} borrowStatus filter for determining if active borrow
   *      request should be on-going or overdue
   * @param {Object} res object that will send the response back to its origin
   * @return {null} returns null on success
   */
  async searchLent(req, res) {
    try {
      const searchQuery = req.query.searchQuery
        ? req.query.searchQuery
        : ''
      const pageNumber = req.query.pageNumber
        ? req.query.pageNumber
        : 1
      const recordsPerPage = req.query.recordsPerPage
        ? req.query.recordsPerPage
        : 20
      const borrowStatus = req.query.borrowStatus
          ? req.query.borrowStatus
          : false

      var modelMappingArray = this.Borrow.modelMappingToArray({
        user_id: false,
        resource_id: false
      })
      const userInclusion = this.User.getInclusionAttributes()
      const resourceInclusion = this.Resource.getInclusionAttributes()

      this.Borrow.associateRelatedTables()
      const include = [
        userInclusion,
        resourceInclusion
      ]

      modelMappingArray = this.queryBuilder.pushAssociatedModelMappingForJoined(
        modelMappingArray,
        [
          userInclusion,
          resourceInclusion
        ]
      )

      let queryBody = this.queryBuilder.buildSearchQuery(
        modelMappingArray,
        searchQuery,
        { status: 'lent' }
      )

      if (borrowStatus) {
        queryBody = this.queryBuilder.borrowStatusLent(
          borrowStatus,
          queryBody
        )
      }

      var borrows = {}

      borrows.count = await this.Borrow.count(
        queryBody,
        include
      )
      borrows.rows = await this.Borrow.getAllJoined(
        pageNumber,
        recordsPerPage,
        queryBody,
        include
      )
      const numberOfPages = Math.ceil(borrows.count / recordsPerPage)

      res.send({
        success: true,
        numberOfPages,
        data: this.decorateJoinedBorrowRows(borrows)
      })
    } catch (err) {
      errorHandler.handle(err, req, res)
    }
  }

  /**
   * Reads borrow request history filtered by the query parameters.
   *
   * @method getHistory
   * @param {Object} req request object containing:
   *  @prop {Object} query request query parameters containing:
   *    @prop {String} pageNumber pagination offset determinant
   *    @prop {String} recordsPerPage number of rows to be returned
   *    @prop {String} borrowStatus filter for determining if active borrow
   *      request should be on-going or overdue or just rejected/returned
   * @param {Object} res object that will send the response back to its origin
   * @return {null} returns null on success
   */
  async getHistory(req, res) {
    try {
      const pageNumber = req.query.pageNumber
          ? req.query.pageNumber
          : 1
      const recordsPerPage = req.query.recordsPerPage
          ? req.query.recordsPerPage
          : 20
      const borrowStatus = req.query.borrowStatus
          ? req.query.borrowStatus
          : false

      let queryBody = this.queryBuilder.buildOrQueryBody(
        'status',
        [
          'rejected',
          'returned'
        ]
      )

      if (borrowStatus) {
        queryBody = this.queryBuilder.borrowStatusHistory(
          borrowStatus,
          queryBody
        )
      }

      this.Borrow.associateRelatedTables()
      const include = [
        this.User.getInclusionAttributes(),
        this.Resource.getInclusionAttributes()
      ]

      var borrows = {}

      borrows.count = await this.Borrow.count(
        queryBody,
        include
      )
      borrows.rows = await this.Borrow.getAllJoined(
        pageNumber,
        recordsPerPage,
        queryBody,
        include
      )
      const numberOfPages = Math.ceil(borrows.count / recordsPerPage)

      res.send({
        success: true,
        numberOfPages,
        data: this.decorateJoinedBorrowRows(borrows)
      })
    } catch (err) {
      errorHandler.handle(err, req, res)
    }
  }

  /**
   * Reads borrow request history from a search query filtered by the query
   *  parameters.
   *
   * @method searchHistory
   * @param {Object} req request object containing:
   *  @prop {Object} query request query parameters containing:
   *    @prop {String} searchQuery search query for filtering results
   *    @prop {String} pageNumber pagination offset determinant
   *    @prop {String} recordsPerPage number of rows to be returned
   *    @prop {String} borrowStatus filter for determining if active borrow
   *      request should be on-going or overdue or just rejected/returned
   * @param {Object} res object that will send the response back to its origin
   * @return {null} returns null on success
   */
  async searchHistory(req, res) {
    try {
      const searchQuery = req.query.searchQuery
        ? req.query.searchQuery
        : ''
      const pageNumber = req.query.pageNumber
        ? req.query.pageNumber
        : 1
      const recordsPerPage = req.query.recordsPerPage
        ? req.query.recordsPerPage
        : 20
      const borrowStatus = req.query.borrowStatus
          ? req.query.borrowStatus
          : false

      var modelMappingArray = this.Borrow.modelMappingToArray({
        user_id: false,
        resource_id: false
      })
      const userInclusion = this.User.getInclusionAttributes()
      const resourceInclusion = this.Resource.getInclusionAttributes()

      this.Borrow.associateRelatedTables()
      const include = [
        userInclusion,
        resourceInclusion
      ]

      modelMappingArray = this.queryBuilder.pushAssociatedModelMappingForJoined(
        modelMappingArray,
        [
          userInclusion,
          resourceInclusion
        ]
      )

      let queryBody = this.queryBuilder.buildOrSearchQuery(
        modelMappingArray,
        searchQuery,
        {
          column: 'status',
          values: [
            'rejected',
            'returned'
          ]
        }
      )

      if (borrowStatus) {
        queryBody = this.queryBuilder.borrowStatusHistory(
          borrowStatus,
          queryBody
        )
      }

      var borrows = {}

      borrows.count = await this.Borrow.count(
        queryBody,
        include
      )
      borrows.rows = await this.Borrow.getAllJoined(
        pageNumber,
        recordsPerPage,
        queryBody,
        include
      )
      const numberOfPages = Math.ceil(borrows.count / recordsPerPage)

      res.send({
        success: true,
        numberOfPages,
        data: this.decorateJoinedBorrowRows(borrows)
      })
    } catch (err) {
      errorHandler.handle(err, req, res)
    }
  }

  /**
   * Updates borrow request column values.
   *
   * @method updateBorrow
   * @param {Object} req request object containing:
   *  @prop {Object} params URL parameters containing:
   *    @prop {String} borrow_id
   *  @prop {Object} body JSON body parameters containing update borrow request
   *    operation variables
   * @param {Object} res object that will send the response back to its origin
   * @return {null} returns null on success
   */
  async updateBorrow(req, res) {
    try {
      const { borrow_id } = req.params
      let lastBorrowStatus = ''

      let updatedBorrow = ''

      this.Borrow.associateRelatedTables()
        const include = [
          this.User.getInclusionAttributes(),
          this.Resource.getInclusionAttributes()
      ]

      if ('status' in req.body) {
        const borrowChecker = await this.Borrow.getJoined(
              { id: borrow_id },
              include
          )

        if (req.body.status === 'pending' || req.body.status === 'lent') {
          if (
            borrowChecker.resource.borrowed >= borrowChecker.resource.count &&
            (
              req.body.status === 'pending' ||
              req.body.status === 'lent'
            )
          ) {
            errorHandler.throw('Resource not available for borrowing', 500)
          }
        }
        lastBorrowStatus = borrowChecker.status
      }

      this.validateDateParameters(req.body)

      this.queryBuilder.validateBody(
        this.Borrow.modelMappingToArray({
          user_id: false,
          resource_id: false,
          date_approved: true,
          date_due: true,
          date_returned: true
        }),
        req.body
      )

      const borrow = await this.Borrow.update(borrow_id, req.body)

      if (borrow) {

        updatedBorrow = await this.Borrow.getJoined(
          { id: borrow_id },
          include
        )
      } else {
        errorHandler.throw('Failed to update borrow request', 500)
      }

      if (!updatedBorrow) {
        errorHandler.throw('Borrow request does not exist', 404)
      }

      if ('status' in req.body && lastBorrowStatus !== req.body.status) {
        if (req.body.status === 'lent') {
          await this.Resource.incrementCount(updatedBorrow.resource_id)
          updatedBorrow.resource.borrowed += 1
        } else if (lastBorrowStatus === 'lent') {
          await this.Resource.decrementCount(updatedBorrow.resource_id)
          updatedBorrow.resource.borrowed -= 1
        }
      }

      res.send({
        success: true,
        data: this.decorateJoinedBorrowRow(updatedBorrow)
      })

    } catch (err) {
      errorHandler.handle(err, req, res)
    }
  }

  /**
   * Validates date parameters in request body.
   *
   * @method validateDateParameters
   * @param {Object} requestBody body to be validated
   * @return {null} returns null on success
   */
  validateDateParameters(requestBody) {
    if ('date_approved' in requestBody) {
      if (requestBody.date_approved !== null &&
        !Number.isInteger(parseInt(requestBody.date_approved, 10))) {
          errorHandler.throw('Approval date should be a valid timestamp', 400)
      }
    }

    if ('date_due' in requestBody) {
      if (requestBody.date_due !== null &&
        !Number.isInteger(parseInt(requestBody.date_due, 10))) {
          errorHandler.throw('Due date should be a valid timestamp', 400)
      }
    }

    if ('date_returned' in requestBody) {
      if (requestBody.date_returned !== null &&
        !Number.isInteger(parseInt(requestBody.date_returned, 10))) {
          errorHandler.throw('Return date should be a valid timestamp', 400)
      }
    }
  }

  /**
   * Transforms resource details from string to JSON.
   *
   * @method decorateJoinedBorrowRow
   * @param {Object} body borrow request body
   * @return {Object} returns body
   */
  decorateJoinedBorrowRow(body) {
    body.resource.details = JSON.parse(body.resource.details)

    return body
  }

  /**
   * Transforms all resource details on an array of borrows from string to JSON.
   *
   * @method decorateJoinedBorrowRows
   * @param {Array} borrows array of borrow requests
   * @return {Array} returns borrows
   */
  decorateJoinedBorrowRows(borrows) {
    Object.keys(borrows.rows).forEach((key) => {
        borrows.rows[key] = this.decorateJoinedBorrowRow(borrows.rows[key])
    })

    return borrows
  }

}
