/*eslint-disable */

/**
 * Class used by routers to dynamically import controllers.
 *
 * @class importControllers
 * @constructor
 */
export default class importControllers {
  constructor(api) {
    this.api = api
  }

  /**
   * Called from the router to use the controller needed.
   *
   * @method use
   * @param {String} controllerName name of the controller to be returned
   * @return {Object} returns new controllerClass()
   */
  use(controllerName) {
    const controllerClass = 
      require(`@controllers/${this.api}/${controllerName}`).default
    return new controllerClass()
  }
}
