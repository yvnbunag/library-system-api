/* eslint no-console: off */
import baseWorker from '@workers/baseWorker'
import jobs from './'
import http from 'http'
import https from 'https'
import moment from 'moment'

/**
 * Class that manages the running of workers.
 *
 * @class workerRunner
 * @constructor
 *  @prop {Mixed} work determinant if the worker is running
 *  @prop {Mixed} lastRun timestamp of when the workers are last run
 *  @prop {Mixed} nextRun timestamp of when the workers are scheduled to run
 *    again
 *  @prop {String} runInterval interval in minutes of the job runner
 */
export default class workerRunner extends baseWorker {
  constructor() {
    super()
    this.work = false
    this.lastRun = false
    this.nextRun = false
    this.runInterval = this.getParameters('WORKER_INTERVAL_IN_MINUTES')
  }

  /**
   * Formats last run timestamp into a readable date-time format.
   *
   * @method lastRunFormatted
   * @return {Function} returns formatted last run date
   */
  lastRunFormatted() {
    return this.formatDate(this.lastRun)
  }

  /**
   * Updates nextRun property.
   *
   * @method updateNextRun
   * @return {null} returns null on success
   */
  updateNextRun() {
    const nextRun = moment(this.lastRun).add(this.runInterval, 'minutes')

    this.nextRun = nextRun
  }

  /**
   * Formats next run timestamp into a readable date-time format.
   *
   * @method nextRunFormatted
   * @return {String} returns formatted next run date
   */
  nextRunFormatted() {
    const nextRunFormatted = this.formatDate(this.nextRun)
    let nextRunMinutes = moment(this.nextRun)
        .diff(Date.now())

    nextRunMinutes = parseInt(moment(nextRunMinutes)
        .format('m')
        .toString(), 10) + 1

    return `${nextRunFormatted} (${nextRunMinutes} minute/s)`
  }

  /**
   * Starts the worker runner.
   *
   * @method startWorker
   * @return {null} returns null on success
   */
  async startWorker() {
    const workerInterval =
      this.runInterval * 60 * 1000

    this.lastRun = Date.now()
    this.updateNextRun()
    this.work = setInterval(async () => {
      const ping = await this.pingAPI()

      if (ping) {
        this.logWork('API ping successful')
      }

      const result = await this.executeJobs()

      this.logWork(result)
    }, workerInterval)

    this.logWork('Worker started')

    // First run
    try {
      const result = await this.executeJobs()

      this.logWork(result)
    } catch (err) {
      this.logWork(err.message)
    }
  }

  /**
   * Stops the worker runner.
   *
   * @method stopWorker
   * @return {null} returns null on success
   */
  stopWorker() {
    clearInterval(this.work)
    this.work = false
    this.logWork('Worker stopped')
  }

  /**
   * Returns the details about the status of the worker runner.
   *
   * @method workerStatus
   * @return {Boolean} returns worker runner status
   */
  workerStatus() {
    if (!this.work) {
      return false
    }

    return true
  }

  /**
   * Trigger for the execution of worker jobs.
   *
   * @method executeJobs
   * @param {Boolean} manual determines if the method has been called manually
   * @return {null} returns null on success
   */
  executeJobs(manual = false) {
    this.logWork('Job runner started')
    this.lastRun = Date.now()
    if (!manual) {
      this.updateNextRun()
    }

    Object.keys(jobs).forEach(async (index) => {
        try {
          const finish = await jobs[index].execute()

          this.logWork(finish)
        } catch (err) {
          return this.logCustomMessage(`Job failed: ${err.message}`)
        }
    })

    if (manual) {
      return this.logCustomMessage('Manual job run initialized')
    }

    return this.logCustomMessage('Worker job run initialized')
  }

  /**
   * Pings the API while the worker is running to prevent auto sleep (Heroku
   *  Dyno setting).
   *
   * @method pingAPI
   * @return {Function} returns get request to self
   */
  pingAPI() {
    const url = new URL(process.env.API_URL)
    let client = http

    if (url.toString().indexOf('https') === 0) {
      client = https
    }

    return client.get(url)
  }

  /**
   * Formats any date object into a readable date-time format.
   *
   * @method formatDate
   * @param {Object} date date to be formatted
   * @return {String} returns formatted date-time
   */
  formatDate(date) {
    return moment(date)
        .utcOffset(480)
        .format('MMMM DD, YYYY [at] hh:mma')
        .toString()
  }

  /**
   * Customized logging for individual worker status.
   *
   * @method logWork
   * @param {String} message message to be logged
   * @return {null} returns null on success
   */
  logWork(message) {
    console.log(`[Worker] ${message} on ${this.formatDate(new Date())}`)
  }

  /**
   * Customized log message for the job runner.
   *
   * @method logCustomMessage
   * @param {String} message message to be logged
   * @return {String} returns customized message with timestamp
   */
  logCustomMessage(message) {
    return `${message} on ${this.formatDate(new Date())}`
  }
}
