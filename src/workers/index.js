import BorrowExpiryWorker from './borrowExpiryWorker'
import BorrowPenalizedWorker from './borrowPenalizedWorker'
import BorrowWarningWorker from './borrowWarningWorker'

/*
 * Instantiation of all workers to be run.
 */
export default {
  borrowExpiryWorker: new BorrowExpiryWorker(),
  borrowPenalizedWorker: new BorrowPenalizedWorker(),
  borrowWarningWorker: new BorrowWarningWorker()
}
