import YmlLoader from '@lib/ymlLoader'

/**
 * Superclass for all worker classes.
 *
 * @class baseWorker
 */
export default class baseWorker {

  /**
   * Extracts parameter from parameters config file.
   *
   * @method getParameters
   * @param {String} paramName name of the parameter to be extracted
   * @return {String} returns parameter value
   */
  getParameters(paramName) {
    const parameters = new YmlLoader().loadGlobalConfig('parameters')

    return parameters[paramName]
  }

  /**
   * Reads a borrow setting from config file.
   *
   * @method getBorrowSettings
   * @param {String }settingsName borrow setting key to be returned
   * @return {Object} returns borrowSettings
   */
  getBorrowSettings(settingsName) {
    const borrowSettings = new YmlLoader().loadGlobalConfig('borrow.settings')

    return borrowSettings[settingsName]
  }
}
