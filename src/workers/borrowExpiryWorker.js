import baseWorker from '@workers/baseWorker'
import borrowModel from '@models/borrowModel'
import operatorAliases from '@models/config/sequelize.operatorAliases'

/**
 * Worker class that deletes expired user borrow requests.
 *
 * @class borrowExpiryWorker
 * @constructor
 */
export default class borrowExpiryWorker extends baseWorker {
  constructor() {
    super()
    this.Borrow = new borrowModel()
  }

  /**
   * Method to execute the job of the worker to be called by the worker runner.
   *
   * @method execute
   * @return {String} returns job execution result
   */
  async execute() {
    const expiryDays =
      this.getBorrowSettings('BORROW_REQUEST_EXPIRY_IN_DAYS') * 60 * 60 * 24
    const expiredDate = Math.floor(Date.now() / 1000) - expiryDays
    const utcExpiredDate = new Date(expiredDate * 1000).toUTCString()

    try {
      await this.Borrow.delete({
        status: 'pending',
        created_at: {
          [operatorAliases.$lte]: utcExpiredDate
        }
      })

      return 'borrowExpiryWorker job finished'
    } catch (err) {
      return `borrowExpiryWorker failed: ${err.message}`
    }
  }
}
