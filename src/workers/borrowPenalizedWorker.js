/* eslint
  prefer-template: off,
  global-require: off,
  no-undef: off,
  no-console: off,
  function-paren-newline: off,
  no-useless-concat: off,
  max-lines-per-function: off,
  no-unused-vars: off
*/
import baseWorker from '@workers/baseWorker'
import borrowModel from '@models/borrowModel'
import usersModel from '@models/usersModel'
import resourcesModel from '@models/resourcesModel'
import operatorAliases from '@models/config/sequelize.operatorAliases'

/**
 * Worker class that notifies users that have overdue borrows via email.
 *
 * @class borrowPenalizedWorker
 * @constructor
 */
export default class borrowPenalizedWorker extends baseWorker {
  constructor() {
    super()
    this.Borrow = new borrowModel()
    this.User = new usersModel()
    this.Resource = new resourcesModel()
  }

  /**
   * Method to execute the job of the worker to be called by the worker runner.
   *
   * @method execute
   * @return {String} returns job execution result
   */
  async execute() {
    if (process.env.MAILING_SERVICE === 'true') {
      const workerInterval =
        this.getParameters('WORKER_INTERVAL_IN_MINUTES') * 60

      try {
        const reminderEndDate = Math.floor(Date.now() / 1000)
        const reminderStartDate = reminderEndDate - workerInterval

        this.Borrow.associateRelatedTables()
        const include = [
          this.User.getInclusionAttributes(),
          this.Resource.getInclusionAttributes()
        ]

        const overdueBorrows = await this.Borrow.getAllJoinedNoLimit(
          {
            status: 'lent',
            [operatorAliases.$and]: [
              {
                date_due: {
                  [operatorAliases.$gte]: reminderStartDate
                }
              },
              {
                date_due: {
                  [operatorAliases.$lte]: reminderEndDate
                }
              }
            ]
          },
          include
        )

        const defaultSender = process.env.MAILING_SERVICE_DEFAULT_SENDER


        overdueBorrows.forEach((borrow) => {
          const helper = require('sendgrid').mail
          const from_email = new helper.Email(defaultSender, 'Library System')
          const to_email = new helper.Email(borrow.user.email)
          const subject = 'Borrowed document overdue'
          const borrowDetails = JSON.parse(borrow.resource.details)
          let borrowDetailsMailFormat = ''

          Object.keys(borrowDetails).forEach((key) => {
            borrowDetailsMailFormat +=
              `${this.formatDetail(key)}: ${borrowDetails[key]} <br>`
          })

          const content = new helper.Content(
            'text/html',
            `Hi ${borrow.user.first_name},` + '<br><br>' +
            'You have a document that you have borrowed from the library ' +
            'that is overdue. Details about the document are as follows:' +
            '<br><br>' +
            `Title: ${borrow.resource.title}` + '<br>' +
            borrowDetailsMailFormat +
            `Type: ${borrow.resource.type}` + '<br><br>' +
            'It is advised to return the borrowed document immediately for ' +
            `you would be charged ${this.getPenaltyCost()} each day that the ` +
            'item is overdue. For more details, visit your account ' +
            `<a href="${process.env.APPLICATION_URL}">here</a>.` + '<br><br>' +
            '<span style = "color: gray;"> This is a notification from ' +
            'the library system,  replies are not supported.</span>'
          )
          const mail = new helper.Mail(from_email, subject, to_email, content)

          const sg = require('sendgrid')(process.env.SENDGRID_API_KEY)
          const request = sg.emptyRequest({
            method: 'POST',
            path: '/v3/mail/send',
            body: mail.toJSON()
          })

          sg.API(request, function(error, response) {
            if (error) {
              console.log(
                'Failed to send penalty notice notification email to ' +
                borrow.user.email
              )
            }
              console.log(
                'Successfully sent penalty notice notification email to ' +
                borrow.user.email
              )
          })
        })

        return 'borrowPenalizedWorker job finished'
      } catch (err) {
        return `borrowPenalizedWorker failed: ${err.message}`
      }
    }

    return 'borrowPenalizedWorker skipped(no mailing service)'
  }

  /**
   * Transforms detail key to readable format.
   *
   * @method formatDetail
   * @param {String} string key to be formatted
   * @return {String} returns formattedDetail
   */
  formatDetail(string) {
    let formattedDetail = string.charAt(0).toUpperCase() + string.slice(1)

    formattedDetail = formattedDetail.replace('_', ' ')

    return formattedDetail
  }

  /**
   * Returns penalty cost of overdue borrows in Pesos.
   *
   * @method getPenaltyCost
   * @return {String} returns penalty cost
   */
  getPenaltyCost() {
    const penaltyCost = this.getBorrowSettings('LENT_RESOURCE_PENALTY_IN_PESO')

    return `&#8369; ${penaltyCost.toFixed(2, 10)}`
  }
}
