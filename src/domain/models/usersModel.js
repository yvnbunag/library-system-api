import Sequelize from 'sequelize'
import baseModel from '@models/baseModel'
import BorrowModel from '@models/borrowModel'

/**
 * Object representation of users table.
 *
 * @class Users
 * @constructor
 */
export default class Users extends baseModel {
  constructor() {
    super()
    // Model settings
    this.modelName = 'users'
    this.modelMapping = {
        first_name: {
          type: Sequelize.STRING(30),
          allowNull: false
        },
        middle_initial: {
          type: Sequelize.STRING(30),
          allowNull: false
        },
        last_name: {
          type: Sequelize.STRING(30),
          allowNull: false
        },
        email: {
          type: Sequelize.STRING(50),
          allowNull: false
        },
        username: {
          type: Sequelize.STRING,
          allowNull: false
        },
        password: {
          type: Sequelize.STRING,
          allowNull: false
        },
        type: {
          type: Sequelize.STRING(20),
          allowNull: false
        },
        status: {
          type: Sequelize.STRING(20),
          allowNull: false
        }
      }
    this.modelConfig = {
      timestamps: false,
      underscored: true
    }
    // Model initialization
    this.model = this.sequelize.define(
      this.modelName,
      this.modelMapping,
      this.modelConfig
    )
  }

  /**
   * Validates if credentials match in the database.
   *
   * @method validate
   * @param {String} username username of the user
   * @param {String} password encrypted password of the user
   * @return {Function} returns Sequelize.findOne as callback function
   */
  validate(username, password) {
    return this.model.findOne({
      where: {
        username,
        password
      },
      raw: true
    })
  }

  /**
   * Validates if credentials match in the database with id as additional field.
   *
   * @method middlewareValidate
   * @param {String} id id of the user
   * @param {String} username username of the user
   * @param {String} password encrypted password of the user
   * @return {Function} returns Sequelize.findOne as callback function
   */
  middlewareValidate(id = null, username = null, password = null) {
    return this.model.findOne({
      where: {
        id,
        username,
        password
      },
      raw: true
    })
  }

  /**
   * Creates user.
   *
   * @method create
   * @param {Object} clause object containing create user details
   * @return {Function} returns Sequelize.create as callback function
   */
  create(clause) {
    return this.model.create(clause)
  }

  /**
   * Creates/Updates user determined by whether the username is already taken.
   *
   * @method createOrUpdateUser
   * @param {Object} userDetails object containing create user details
   * @return {Function} returns Sequelize.create/Sequelize.update as callback
   *  function
   */
  createOrUpdateUser(userDetails) {
    return this.model.findOne({
      where: {
        username: userDetails.username
      }
    }).then((user) => {
      if (user === null) {
        return this.model.create(userDetails)
      }

      return user.update(userDetails)
    })
  }

  /**
   * Reads specific user columns.
   *
   * @method get
   * @param {Object} filters object containing filters for the query
   * @return {Function} returns Sequelize.findOne as callback function
   */
  get(filters = {}) {
    return this.model.findOne({
      where: filters,
      raw: true
    })
  }

  /**
   * Reads specific user columns joined with its associated tables.
   *
   * @method getJoined
   * @param {Object} filters object containing filters for the query
   * @param {Array} include array of associated models to be joined in the query
   * @return {Function} returns Sequelize.findOne as callback function
   */
  getJoined(filters = {}, include = []) {
    const raw = include === []

    return this.model.findOne({
      where: filters,
      include,
      raw
    })
  }

  /**
   * Reads a set of user details.
   *
   * @method getAll
   * @param {String} pageNumber pagination offset determinant
   * @param {String} recordsPerPage number of rows to be returned
   * @param {Object} filters object containing filters for the query
   * @param {Array} order array containing result order for the query
   * @return {Function} returns Sequelize.findAndCountAll as callback function
   */
  getAll(
    pageNumber = 1,
    recordsPerPage = 20,
    filters = {},
    order = [
      [
        'id',
        'DESC'
      ]
    ]
  ) {
    const offset = (parseInt(pageNumber, 10) - 1) * parseInt(recordsPerPage, 10)
    const limit = parseInt(recordsPerPage, 10)

    return this.model.findAndCountAll({
      where: filters,
      raw: true,
      attributes: {
        exclude: ['password']
      },
      offset,
      limit,
      order
    })
  }

  /**
   * Reads a set of user details joined with its associated tables.
   *
   * @method getAllJoined
   * @param {String} pageNumber pagination offset determinant
   * @param {String} recordsPerPage number of rows to be returned
   * @param {Object} filters object containing filters for the query
   * @param {Array} include array of associated models to be joined in the query
   * @param {Array} order array containing result order for the query
   * @return {Function} returns Sequelize.findAndCountAll as callback function
   */
  getAllJoined(
    pageNumber = 1,
    recordsPerPage = 20,
    filters = {},
    include = [],
    order = [
      [
        'id',
        'DESC'
      ]
    ]
  ) {
    const offset = (parseInt(pageNumber, 10) - 1) * parseInt(recordsPerPage, 10)
    const limit = parseInt(recordsPerPage, 10)
    const raw = include === []

    return this.model.findAndCountAll({
      where: filters,
      include,
      raw,
      attributes: {
        exclude: ['password']
      },
      offset,
      limit,
      order
    })
  }

  /**
   * Reads all user details excluding id field for exporting.
   *
   * @method getAllExport
   * @return {Function} returns Sequelize.findAll as callback function
   */
  getAllExport() {
    const order = [
      [
        'id',
        'ASC'
      ]
    ]

    return this.model.findAll({
      raw: true,
      attributes: {
        exclude: ['id']
      },
      order
    })
  }

  /**
   * Updates existing user.
   *
   * @method update
   * @param {String} id id of user
   * @param {Object} clause object containing update user details
   * @return {Function} returns Sequelize.update as callback function
   */
  update(id, clause) {
    return this.model.update(
      clause,
      { where: { id } }
    )
  }

  /**
   * Deletes existing user.
   *
   * @method delete
   * @param {Object} clause object containing delete user conditions
   * @return {Function} returns Sequelize.destroy as callback function
   */
  delete(clause) {
    return this.model.destroy({
      where: clause
    })
  }

  /**
   * Counts number of rows based on a user table query.
   *
   * @method count
   * @param {Object} filters object containing filters for the query
   * @param {Array} include array of associated models to be joined in the query
   * @return {Function} returns Sequelize.count as callback function
   */
  count(filters = {}, include = []) {
    return this.model.count({
      where: filters,
      include,
      raw: true
    })
  }

  /**
   * Associates related tables for use in joined queries.
   *
   * @method associateRelatedTables
   * @return {null} returns null on success
   */
  associateRelatedTables() {
    const Borrow = new BorrowModel()

    this.model.hasMany(Borrow.model)
  }

  /**
   * Returns user model inclusion attributes to be used in joined queries.
   *
   * @method getInclusionAttributes
   * @return {Object} returns user model and array of allowed attributes
   */
  getInclusionAttributes() {
    return {
      model: this.model,
      attributes: [
        'first_name',
        'middle_initial',
        'last_name',
        'email',
        'username'
      ]
    }
  }

  /**
   * Returns the user model.
   *
   * @method model
   * @return {Object} returns user model
   */
  model() {
    return this.model
  }

  /**
   * Transforms the user model's mapping into an array.
   *
   * @method modelMappingToArray
   * @param {Object} settings collection of attributes to be included/excluded
   *  in the return object
   * @return {Object} returns modelArray
   */
  modelMappingToArray(settings = {
      id: undefined,
      username: undefined,
      password: undefined
    }) {
    const { modelMapping } = this
    var modelArray = []

    Object.keys(modelMapping).forEach(function(key) {
      modelArray[key] = {
          length: modelMapping[key].type._length,
          allowNull: modelMapping[key].allowNull
      }
    })

    if (settings.id && settings.id !== undefined) {
      modelArray.id = {
        length: 255,
        allowNull: true
      }
    }

    if (!settings.username && settings.username !== undefined) {
      Reflect.deleteProperty(modelArray, 'username')
    }

    if (!settings.password || settings.password === undefined) {
      Reflect.deleteProperty(modelArray, 'password')
    }

    return modelArray
  }
}
