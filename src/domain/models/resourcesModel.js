import Sequelize from 'sequelize'
import baseModel from '@models/baseModel'
import BorrowModel from '@models/borrowModel'

/**
 * Object representation of resources table.
 *
 * @class Resources
 * @constructor
 */
export default class Resources extends baseModel {
  constructor() {
    super()
    // Model settings
    this.modelName = 'resources'
    this.modelMapping = {
        title: {
          type: Sequelize.STRING(100),
          allowNull: false
        },
        details: {
          type: Sequelize.STRING(510),
          allowNull: false
        },
        count: {
          type: Sequelize.INTEGER,
          allowNull: true
        },
        borrowed: {
          type: Sequelize.INTEGER,
          allowNull: true
        },
        classification: {
          type: Sequelize.STRING(15),
          allowNull: false
        },
        type: {
          type: Sequelize.STRING(15),
          allowNull: false
        }
      }
    this.modelConfig = {
      timestamps: false,
      underscored: true
    }
    // Model initialization
    this.model = this.sequelize.define(
      this.modelName,
      this.modelMapping,
      this.modelConfig
    )
  }

  /**
   * Creates resource.
   *
   * @method create
   * @param {Object} clause object containing create resource details
   * @return {Function} returns Sequelize.create as callback function
   */
  create(clause) {
    return this.model.create(clause)
  }

  /**
   * Reads specific resource columns.
   *
   * @method get
   * @param {Object} filters object containing filters for the query
   * @return {Function} returns Sequelize.findOne as callback function
   */
  get(filters = {}) {
    return this.model.findOne({
      where: filters,
      raw: true
    })
  }

  /**
   * Reads specific resource columns joined with its associated tables.
   *
   * @method getJoined
   * @param {Object} filters object containing filters for the query
   * @param {Array} include array of associated models to be joined in the query
   * @return {Function} returns Sequelize.findOne as callback function
   */
  getJoined(filters = {}, include = []) {
    const raw = include === []

    return this.model.findOne({
      where: filters,
      include,
      raw
    })
  }

  /**
   * Reads a set of resource details.
   *
   * @method getAll
   * @param {String} pageNumber pagination offset determinant
   * @param {String} recordsPerPage number of rows to be returned
   * @param {Object} filters object containing filters for the query
   * @param {Array} order array containing result order for the query
   * @return {Function} returns Sequelize.findAndCountAll as callback function
   */
  getAll(
    pageNumber = 1,
    recordsPerPage = 20,
    filters = {},
    order = [
        [
          'id',
          'DESC'
        ]
    ]
  ) {
    const offset = (parseInt(pageNumber, 10) - 1) * parseInt(recordsPerPage, 10)
    const limit = parseInt(recordsPerPage, 10)

    return this.model.findAndCountAll({
      where: filters,
      raw: true,
      offset,
      limit,
      order
    })
  }

  /**
   * Reads a set of resource details joined with its associated tables.
   *
   * @method getAllJoined
   * @param {String} pageNumber pagination offset determinant
   * @param {String} recordsPerPage number of rows to be returned
   * @param {Object} filters object containing filters for the query
   * @param {Array} include array of associated models to be joined in the query
   * @param {Array} order array containing result order for the query
   * @return {Function} returns Sequelize.findAndCountAll as callback function
   */
  getAllJoined(
    pageNumber = 1,
    recordsPerPage = 20,
    filters = {},
    include = [],
    order = [
        [
          'id',
          'DESC'
        ]
    ]
  ) {
    const offset = (parseInt(pageNumber, 10) - 1) * parseInt(recordsPerPage, 10)
    const limit = parseInt(recordsPerPage, 10)
    const raw = include === []

    return this.model.findAndCountAll({
      where: filters,
      include,
      raw,
      offset,
      limit,
      order
    })
  }

  /**
   * Reads all resource details filtered and joined with its associated tables
   *  for generation of excel report.
   *
   * @method generateReport
   * @param {Array} attributes collection of fields to be included in the result
   * @param {Object} filters object containing filters for the query
   * @param {Array} include array of associated models to be joined in the query
   * @param {Array} group collection of row group attributes
   * @param {Array} order array containing result order for the query
   * @return {Function} returns Sequelize.findAll as callback function
   */
  generateReport(
    attributes = [],
    filters = {},
    include = [],
    group = [],
    order = [
        [
          'id',
          'ASC'
        ]
    ]
  ) {
    const raw = include === []

    return this.model.findAll({
      attributes,
      where: filters,
      include,
      raw,
      order,
      group
    })
  }

  /**
   * Updates existing resource.
   *
   * @method update
   * @param {String} id id of resource
   * @param {Object} clause object containing update resource details
   * @return {Function} returns Sequelize.update as callback function
   */
  update(id, clause) {
    return this.model.update(
      clause,
      { where: { id } }
    )
  }

  /**
   * Increments count value of a resource.
   *
   * @method incrementCount
   * @param {String} id id of resource=
   * @return {Function} returns Sequelize.update as callback function
   */
  incrementCount(id) {
    return this.model.update(
      {
        borrowed: Sequelize.literal('borrowed + 1')
      },
      {
        where: {
          id
        }
      }
    )
  }

  /**
   * Decrements count value of a resource.
   *
   * @method decrementCount
   * @param {String} id id of resource=
   * @return {Function} returns Sequelize.update as callback function
   */
  decrementCount(id) {
    return this.model.update(
      {
        borrowed: Sequelize.literal('borrowed - 1')
      },
      {
        where: {
          id
        }
      }
    )
  }

  /**
   * Deletes existing resource.
   *
   * @method delete
   * @param {Object} clause object containing delete resource conditions
   * @return {Function} returns Sequelize.destroy as callback function
   */
  delete(clause) {
    return this.model.destroy({
      where: clause
    })
  }

  /**
   * Counts number of rows based on a resource table query.
   *
   * @method count
   * @param {Object} filters object containing filters for the query
   * @param {Array} include array of associated models to be joined in the query
   * @return {Function} returns Sequelize.count as callback function
   */
  count(filters = {}, include = []) {
    return this.model.count({
      where: filters,
      include,
      raw: true
    })
  }

  /**
   * Associates related tables for use in joined queries.
   *
   * @method associateRelatedTables
   * @return {null} returns null on success
   */
  associateRelatedTables() {
    const Borrow = new BorrowModel()

    this.model.hasMany(Borrow.model)
  }

  /**
   * Returns resource model inclusion attributes to be used in joined queries.
   *
   * @method getInclusionAttributes
   * @return {Object} returns resource model and array of allowed attributes
   */
  getInclusionAttributes() {
    return {
      model: this.model,
      attributes: [
        'title',
        'details',
        'count',
        'borrowed',
        'type'
      ]
    }
  }

  /**
   * Returns the resource model.
   *
   * @method model
   * @return {Object} returns resource model
   */
  model() {
    return this.model
  }

  /**
   * Transforms the resource model's mapping into an array.
   *
   * @method modelMappingToArray
   * @param {Object} settings collection of attributes to be included/excluded
   *  in the return object
   * @return {Object} returns modelArray
   */
  modelMappingToArray(settings = {
      id: undefined
    }) {
    const { modelMapping } = this
    var modelArray = []

    Object.keys(modelMapping).forEach(function(key) {
      modelArray[key] = {
          length: modelMapping[key].type._length,
          allowNull: modelMapping[key].allowNull
      }
    })

    if (settings.id && settings.id !== undefined) {
      modelArray.id = {
        length: 255,
        allowNull: true
      }
    }

    return modelArray
  }
}
