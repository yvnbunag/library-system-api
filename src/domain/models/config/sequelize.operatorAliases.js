import { Op } from 'sequelize'

 /*
  * Protects sequelize query operators by representing them in aliases.
  */
const operatorsAliases = {
      $and: Op.and,
      $or: Op.or,
      $eq: Op.eq,
      $gt: Op.gt,
      $lt: Op.lt,
      $gte: Op.gte,
      $lte: Op.lte,
      $like: Op.like,
      $not: Op.not,
      $notLike: Op.notLike
}

export default operatorsAliases
