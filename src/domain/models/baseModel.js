/* eslint max-len: ["error", 85] */
import Sequelize from 'sequelize'
import YmlLoader from '@lib/ymlLoader'
import operatorAliases from '@models/config/sequelize.operatorAliases'

/**
 * Superclass for use of models.
 *
 * @class baseModel
 * @constructor
 */
export default class baseModel {
  constructor() {
    // Sequelize settings
    const ymlLoader = new YmlLoader()
    const configFile = `${__dirname}/config/sequelize.config.${process.env.NODE_ENV}`
    const configSettings = ymlLoader.loadConfig(configFile)
    const defaults = {
        host: process.env.DB_HOST,
        dialect: process.env.DB_DIALECT,
        operatorAliases
      }
    const settings = Object.assign(defaults, configSettings.config)

    // Sequelize initialization
    this.sequelize = new Sequelize(
      process.env.DB,
      process.env.DB_USER,
      process.env.DB_PASSWORD,
      settings
    )
  }

  /**
   * Syncs database table columns from its object representations.
   *
   * @method sync
   * @param {Object} settings settings to be passed during database sync.
   * @return {Function} returns database sync as callback
   */
  sync(settings = { force: false }) {
    return this.model.sync(settings)
  }
}

