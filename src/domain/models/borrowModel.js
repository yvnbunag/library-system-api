import Sequelize from 'sequelize'
import baseModel from '@models/baseModel'
import UsersModel from '@models/usersModel'
import ResourcesModel from '@models/resourcesModel'

/**
 * Object representation of borrow table.
 *
 * @class Borrow
 * @constructor
 */
export default class Borrow extends baseModel {
  constructor() {
    super()
    // Model settings
    this.modelName = 'borrow'
    this.modelMapping = {
        user_id: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        resource_id: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        status: {
          type: Sequelize.STRING(10),
          allowNull: false
        },
        date_approved: {
          type: Sequelize.STRING(20),
          allowNull: true
        },
        date_due: {
          type: Sequelize.STRING(20),
          allowNull: true
        },
        date_returned: {
          type: Sequelize.STRING(20),
          allowNull: true
        }
      }
    this.modelConfig = {
      freezeTableName: true,
      timestamps: true,
      underscored: true
    }
    // Model initialization
    this.model = this.sequelize.define(
      this.modelName,
      this.modelMapping,
      this.modelConfig
    )
  }

  /**
   * Creates borrow.
   *
   * @method create
   * @param {Object} clause object containing create borrow details
   * @return {Function} returns Sequelize.create as callback function
   */
  create(clause) {
    return this.model.create(clause)
  }

  /**
   * Reads specific borrow columns.
   *
   * @method get
   * @param {Object} filters object containing filters for the query
   * @return {Function} returns Sequelize.findOne as callback function
   */
  get(filters = {}) {
    return this.model.findOne({
      where: filters,
      raw: true
    })
  }

  /**
   * Reads specific borrow columns joined with its associated tables.
   *
   * @method getJoined
   * @param {Object} filters object containing filters for the query
   * @param {Array} include array of associated models to be joined in the query
   * @return {Function} returns Sequelize.findOne as callback function
   */
  getJoined(filters = {}, include = []) {
    const raw = include === []

    return this.model.findOne({
      where: filters,
      include,
      raw
    })
  }

  /**
   * Reads a set of borrow details.
   *
   * @method getAll
   * @param {String} pageNumber pagination offset determinant
   * @param {String} recordsPerPage number of rows to be returned
   * @param {Object} filters object containing filters for the query
   * @param {Array} order array containing result order for the query
   * @return {Function} returns Sequelize.findAndCountAll as callback function
   */
  getAll(
      pageNumber = 1,
      recordsPerPage = 20,
      filters = {},
      order = [
        [
          'updated_at',
          'DESC'
        ]
      ]
    ) {
    const offset = (parseInt(pageNumber, 10) - 1) * parseInt(recordsPerPage, 10)
    const limit = parseInt(recordsPerPage, 10)

    return this.model.findAndCountAll({
      where: filters,
      raw: true,
      offset,
      limit,
      order
    })
  }

  /**
   * Reads a set of borrow details joined with its associated tables.
   *
   * @method getAllJoined
   * @param {String} pageNumber pagination offset determinant
   * @param {String} recordsPerPage number of rows to be returned
   * @param {Object} filters object containing filters for the query
   * @param {Array} include array of associated models to be joined in the query
   * @param {Array} order array containing result order for the query
   * @return {Function} returns Sequelize.findAll as callback function
   */
  getAllJoined(
      pageNumber = 1,
      recordsPerPage = 20,
      filters = {},
      include = [],
      order = [
        [
          'updated_at',
          'DESC'
        ]
      ]
    ) {
    const offset = (parseInt(pageNumber, 10) - 1) * parseInt(recordsPerPage, 10)
    const limit = parseInt(recordsPerPage, 10)
    const raw = include === []

    return this.model.findAll({
      where: filters,
      include,
      raw,
      offset,
      limit,
      order
    })
  }

  /**
   * Reads all borrow details joined with its associated tables for use in
   *  worker email notifications.
   *
   * @method getAllJoinedNoLimit
   * @param {Object} filters object containing filters for the query
   * @param {Array} include array of associated models to be joined in the query
   * @param {Array} order array containing result order for the query
   * @return {Function} returns Sequelize.findAll as callback function
   */
  getAllJoinedNoLimit(
      filters = {},
      include = [],
      order = [
        [
          'updated_at',
          'DESC'
        ]
      ]
    ) {
    const raw = include === []

    return this.model.findAll({
      where: filters,
      include,
      raw,
      order
    })
  }

  /**
   * Updates existing borrow.
   *
   * @method update
   * @param {String} id id of borrow
   * @param {Object} clause object containing update borrow details
   * @return {Function} returns Sequelize.update as callback function
   */
  update(id, clause) {
    return this.model.update(
      clause,
      { where: { id } }
    )
  }

  /**
   * Deletes existing borrow.
   *
   * @method delete
   * @param {Object} clause object containing delete borrow conditions
   * @return {Function} returns Sequelize.destroy as callback function
   */
  delete(clause) {
    return this.model.destroy({
      where: clause
    })
  }

  /**
   * Counts number of rows based on a borrow table query.
   *
   * @method count
   * @param {Object} filters object containing filters for the query
   * @param {Array} include array of associated models to be joined in the query
   * @return {Function} returns Sequelize.count as callback function
   */
  count(filters = {}, include = []) {
    return this.model.count({
      where: filters,
      include,
      raw: true
    })
  }

  /**
   * Associates related tables for use in joined queries.
   *
   * @method associateRelatedTables
   * @return {null} returns null on success
   */
  associateRelatedTables() {
    const User = new UsersModel()
    const Resource = new ResourcesModel()

    this.model.belongsTo(User.model)
    this.model.belongsTo(Resource.model)
  }

  /**
   * Returns borrow model inclusion attributes to be used in joined queries.
   *
   * @method getInclusionAttributes
   * @return {Object} returns borrow model and array of allowed attributes
   */
  getInclusionAttributes() {
    return {
      model: this.model,
      attributes: ['resource_id'],
      offset: 0,
      limit: 1
    }
  }

  /**
   * Returns borrow model inclusion attributes to be used in generate report
   *  joined query.
   *
   * @method getInclusionAttributes
   * @return {Object} returns borrow model and array of allowed attributes
   */
  getGenerateReportInclusionAttributes() {
    return {
      model: this.model,
      attributes: [
        'id',
        'status'
      ]
    }
  }

  /**
   * Returns the borrow model.
   *
   * @method model
   * @return {Object} returns borrow model
   */
  model() {
    return this.model
  }

  /**
   * Transforms the borrow model's mapping into an array.
   *
   * @method modelMappingToArray
   * @param {Object} settings collection of attributes to be included/excluded
   *  in the return object
   * @return {Object} returns modelArray
   */
  modelMappingToArray(settings = {
      id: undefined,
      user_id: undefined,
      resource_id: undefined,
      date_approved: undefined,
      date_due: undefined,
      date_returned: undefined
    }) {
    const { modelMapping } = this
    var modelArray = []

    Object.keys(modelMapping).forEach(function(key) {
      modelArray[key] = {
          length: modelMapping[key].type._length,
          allowNull: modelMapping[key].allowNull
      }
    })

    if (settings.id && settings.id !== undefined) {
      modelArray.id = {
        length: 255,
        allowNull: true
      }
    }

    if (!settings.user_id && settings.user_id !== undefined) {
      Reflect.deleteProperty(modelArray, 'user_id')
    }

    if (!settings.resource_id && settings.resource_id !== undefined) {
      Reflect.deleteProperty(modelArray, 'resource_id')
    }

    if (!settings.date_approved || settings.date_approved === undefined) {
      Reflect.deleteProperty(modelArray, 'date_approved')
    }

    if (!settings.date_due || settings.date_due === undefined) {
      Reflect.deleteProperty(modelArray, 'date_due')
    }

    if (!settings.date_returned || settings.date_returned === undefined) {
      Reflect.deleteProperty(modelArray, 'date_returned')
    }

    return modelArray
  }
}
