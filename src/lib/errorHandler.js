/* eslint-disable */
import path from 'path'

/**
 * Class for global error handling.
 *
 * @class errorHandler
 */
export default class errorHandler {

  /**
   * Throws custom error with message and status code.
   *
   * @method methodName
   * @param {String} errorMessage details about the error
   * @param {Int} statusCode status code of the error
   * @throws {Object} throws error object
   */
  throw(errorMessage = null, statusCode = 500) {
    const err = new Error(errorMessage)

    if(process.env.NODE_ENV === 'development') {
      let callerFile;
      let currentFile;

      Error.prepareStackTrace = function (err, stack) { return stack }
      const stack = [...err.stack]
      currentFile = stack.shift().getFileName()

      while (stack.length) {
          callerFile = stack.shift()
          if(currentFile !== callerFile.getFileName()) {
            callerFile = path.parse(callerFile.getFileName()).dir +
            `\\` +
            path.parse(callerFile.getFileName()).name +
              `@${callerFile.getFunctionName()}` +
              `:${callerFile.getLineNumber()}`
            err.trace = callerFile.replace(`${__basedir}\\`,'')
            break
          }
      }
    }

    err.statusCode  = statusCode
    err.source = this
    throw err
  }

  /**
   * Throws custom error with message and status code.
   *
   * @method methodName
   * @param {Object} err error object caught in try-catch clause
   * @param {Object} req request object from origin
   * @param {Object} res object that will send the response back to its origin
   * @param {Function} next function that will pass/skip the execution to the
   *  next middleware
   * @return {null} returns null on success
   */
  handle(err, req, res, next) {
    if (!err) return next()

    const [statusCode, response] = (() => {
      if(
        process.env.NODE_ENV === 'development'
        && err.source instanceof errorHandler
      ) {
        const response = {
          success: false,
          reason: err.message,
          source: err.trace
        }

        return [err.statusCode, response]
      }

      const response = {
        success: false,
        reason: err.message
      }

      return [500, response]
    })()

    res.status(statusCode).send(response)
  }

  /**
   * Response for non existent routes.
   *
   * @method notFound
   * @param {Object} req request object from origin
   * @param {Object} res object that will send the response back to its origin
   * @return {null} returns null on success
   */
  notFound(req, res) {
    res.status(404).send({
      success: false,
      reason: "Not found"
    })
  }
}
