/* eslint max-statements: ["error", 20] prefer-template: "off" */
import operatorAliases from '@models/config/sequelize.operatorAliases'
import Sequelize from 'sequelize'

/**
 * Class for building Sequelize queries.
 *
 * @class queryBuilder
 */
export default class queryBuilder {

  /**
   * Validates query body prior to execution.
   *
   * @method validateBody
   * @param {Array} modelMapping mapping of the model to be used as validator
   * @param {Object} body query body to be validated
   * @param {Object} settings collection of keys to be restricted/excluded
   * @return {null} returns null on success
   */
  validateBody(modelMapping, body, settings = {}) {
    var errorMessage = []

    if (Object.keys(body).length === 0) {
      errorMessage.push('Query body cannot be empty')
    }

    if (Object.keys(settings).length) {
      if (settings.restrict) {
        Object.keys(settings.restrict).forEach((key) => {
          const keyName = settings.restrict[key]

          if (keyName in body) {
            errorHandler.throw(`${keyName} cannot be passed in body`, 500)
          }

          Reflect.deleteProperty(modelMapping, keyName)
        })
      }

      if (settings.exclude) {
        Object.keys(settings.exclude).forEach((key) => {
          Reflect.deleteProperty(modelMapping, settings.exclude[key])
        })
      }

      if (settings.requireAll) {
        Object.keys(modelMapping).forEach((key) => {
          if (body[key] === undefined) {
            errorMessage.push(`Missing parameter '${key}'`)
          }
        })
      }
    }

    Object.keys(body).forEach((key) => {
      if (modelMapping[key] === undefined) {
        errorMessage.push(`Invalid parameter '${key}'`)
      } else if (body[key] !== null &&
          body[key].length > modelMapping[key].length) {
        // Details custom validator
        if (key === 'details') {
          const details = this
            .buildCustomDetailExceedMessage(body)

          errorMessage.push(`Parameter ${key} (${details}) exceeds ` +
            `max length(${modelMapping[key].length})`)
        } else {
          errorMessage.push(`Parameter '${key}' exceeds ` +
            `max length(${modelMapping[key].length})`)
        }
      } else if (!modelMapping[key].allowNull && body[key] === '') {
        errorMessage.push(`Parameter '${key}' cannot be null`)
      }
    })

    if (!Object.keys(errorMessage).length) {
      return null
    }

    errorHandler.throw(errorMessage, 400)
  }

  /**
   * Custom exceed message inclusion for resource details.
   *
   * @method buildCustomDetailExceedMessage
   * @param {Object} body query body with exceeding parameter
   * @return {String} returns detailsCollection
   */
  buildCustomDetailExceedMessage(body) {
    const details = Object.keys(JSON.parse(body.details))
    var detailsCollection = ''

    details.forEach((value, index) => {
      let ucValue = value.charAt(0).toUpperCase() + value.slice(1)

      ucValue = ucValue.replace('_', ' ')

      if (index === details.length - 1) {
        detailsCollection += `and ${ucValue}`
      } else if (index !== details.length - 2) {
        detailsCollection += `${ucValue}, `
      } else {
        detailsCollection += `${ucValue} `
      }

    })

    return `${detailsCollection}`
  }

  /**
   * Builds query body.
   *
   * @method buildQueryBody
   * @param {Array} modelMapping mapping of the model to be used as key base
   * @param {Object} body query body to be built from
   * @return {Object} returns queryBody
   */
  buildQueryBody(modelMapping, body) {
    var queryBody = {}

    Object.keys(body).forEach((key) => {
      if (key in modelMapping) {
        queryBody[key] = body[key]
      }
    })

    return queryBody
  }

  /**
   * Builds a search query body.
   *
   * @method buildSearchQuery
   * @param {Array} mapping mapping of the model to be used as key base
   * @param {String} searchQuery search term to be injected as search query
   * @param {Object} queryBody query body to be built from
   * @return {Object} returns search query body
   */
  buildSearchQuery(mapping, searchQuery, queryBody) {
    const mappedSearchQuery = Object.keys(mapping).map((key) => {
      const mappedObject = {
        [key]: {
          [operatorAliases.$like]: `%${searchQuery}%`
        }
      }

      return mappedObject
    })

    const filters = this.buildQueryBody(
        mapping,
        queryBody
    )

    return {
      [operatorAliases.$and]: [
        {
          [operatorAliases.$or]: mappedSearchQuery
        },
        filters
      ]
    }
  }

  /**
   * Builds an or query body.
   *
   * @method buildOrQueryBody
   * @param {String} column key where 'or' comparison would be applied
   * @param {Array} values collection of comparison values
   * @return {Object} returns an or query body
   */
  buildOrQueryBody(column, values = []) {
    var orCollection = []

    values.forEach((value) => {
        orCollection.push({ [column]: value })
    })

    return {
      [operatorAliases.$or]: orCollection
    }
  }

  /**
   * Builds an or - search query body.
   *
   * @method buildOrSearchQuery
   * @param {Array} mapping mapping of the model to be used as key base
   * @param {String} searchQuery search term to be injected as search query
   * @param {Object} orQueryBody collection of variables to be passed to
   *  `buildOrQueryBody` method for building an or query body
   * @return {Object} returns an or - search query body
   */
  buildOrSearchQuery(mapping, searchQuery, orQueryBody = {}) {
    const mappedSearchQuery = Object.keys(mapping).map((key) => {
      const mappedObject = {
        [key]: {
          [operatorAliases.$like]: `%${searchQuery}%`
        }
      }

      return mappedObject
    })

    const filters = this.buildOrQueryBody(
        orQueryBody.column,
        orQueryBody.values
    )

    return {
      [operatorAliases.$and]: [
        {
          [operatorAliases.$or]: mappedSearchQuery
        },
        filters
      ]
    }
  }

  /**
   * Builds an or query body for borrow requests of a specific user.
   *
   * @method buildBorrowOrQuery
   * @param {String} id user id
   * @param {Array} queryArray collection of or queries
   * @return {Object} returns an or query body
   */
  buildBorrowOrQuery(id, queryArray) {
    return {
      [operatorAliases.$and]: [
        {
          [operatorAliases.$or]: queryArray
        },
        { user_id: id }
      ]
    }
  }

  /**
   * Combines model mappings of associated models for use in joined queries.
   *
   * @method pushAssociatedModelMappingForJoined
   * @param {Array} modelMappingArray base model mapping
   * @param {Array} associations model mappings to be pushed to the base model
   * @return {Array} returns modelMappingArray
   */
  pushAssociatedModelMappingForJoined(
    modelMappingArray = [],
    associations = []
  ) {
    associations.forEach((association) => {
      association.attributes.forEach((attributes) => {
        const associatedAttributeName =
          '$' +
          association.model.name.replace(/s$/, '') +
          '.' +
          attributes +
          '$'

        modelMappingArray[associatedAttributeName] = true
      })
    })

    return modelMappingArray
  }

  /**
   * Appends filters for resource queries.
   *
   * @method buildQueryFilters
   * @param {Object} queryBody query body to be injected with filters
   * @param {Array} includedTypes collection of item types to be included
   * @param {Object} itemMapping mapping of item types/classifications
   * @return {Object} returns filteredBody
   */
  buildQueryFilters(queryBody, includedTypes = [], itemMapping) {
    var itemTypes = []
    let filteredBody = {}
    const isAliased = Object.getOwnPropertySymbols(queryBody)

    Object.keys(itemMapping).forEach((classification) => {
      Object.keys(itemMapping[classification]).forEach((type) => {
        itemTypes.push(type)
      })
    })

    let exclusions = itemTypes.filter((type) => !includedTypes.includes(type))

    exclusions = exclusions.map((excludedType) => ({ type: {
        [operatorAliases.$not]: excludedType
      } }))

    if (isAliased.length) {
      isAliased.forEach((operatorAlias) => {
        filteredBody = queryBody
        filteredBody[operatorAlias].push({ [operatorAliases.$and]: exclusions })
      })
    } else {
      filteredBody[operatorAliases.$and] = [
        queryBody,
        { [operatorAliases.$and]: exclusions }
      ]
    }

    return filteredBody
  }

  /**
   * Appends filter that would/would not require a resource to be in an
   *  available state.
   *
   * @method requireAvailability
   * @param {Boolean} availability availability determinant
   * @param {Object} filteredBody query body to be injected with filter
   * @return {Object} returns filteredBody
   */
  requireAvailability(availability = true, filteredBody) {
    const bodyProperties = Object.getOwnPropertySymbols(filteredBody)

    if (availability) {
      bodyProperties.forEach((operatorAlias) => {
        filteredBody[operatorAlias].push({
          count: {
            [operatorAliases.$gt]: Sequelize.literal('borrowed')
          }
        })

        filteredBody[operatorAlias].push({
          classification: {
            [operatorAliases.$not]: 'digital'
          }
        })
      })
    } else {
      bodyProperties.forEach((operatorAlias) => {
        filteredBody[operatorAlias].push({
          count: {
            [operatorAliases.$lte]: Sequelize.literal('borrowed')
          }
        })

        filteredBody[operatorAlias].push({
          classification: {
            [operatorAliases.$not]: 'digital'
          }
        })
      })
    }

    return filteredBody
  }

  /**
   * Appends filter that would fetch active borrow requests with a given status.
   *
   * @method borrowStatusLent
   * @param {String} status status of the borrow request that would be required
   * @param {Object} queryBody query body to be injected with filter
   * @return {Object} returns statusBody/queryBody
   */
  borrowStatusLent(status, queryBody) {
    const isAliased = Object.getOwnPropertySymbols(queryBody)
    let statusBody = {}
    const nowTimestamp = Math.floor(Date.now() / 1000)

    if (status === 'ongoing') {
      if (isAliased.length) {
        isAliased.forEach((operatorAlias) => {
          statusBody = queryBody
          statusBody[operatorAlias].push({
              date_due: {
                [operatorAliases.$gte]: nowTimestamp
              }
          })
        })
      } else {
        statusBody[operatorAliases.$and] = [
          queryBody,
          {
              date_due: {
                [operatorAliases.$gte]: nowTimestamp
              }
          }
        ]
      }

      return statusBody
    } else if (status === 'overdue') {
      if (isAliased.length) {
        isAliased.forEach((operatorAlias) => {
          statusBody = queryBody
          statusBody[operatorAlias].push({
              date_due: {
                [operatorAliases.$lt]: nowTimestamp
              }
          })
        })
      } else {
        statusBody[operatorAliases.$and] = [
          queryBody,
          {
              date_due: {
                [operatorAliases.$lt]: nowTimestamp
              }
          }
        ]
      }

      return statusBody
    }

    return queryBody
  }

  /**
   * Appends filter that would fetch borrow histories with a given status.
   *
   * @method borrowStatusHistory
   * @param {String} status status of the borrow request that would be required
   * @param {Object} queryBody query body to be injected with filter
   * @return {Object} returns statusBody
   */
  borrowStatusHistory(status, queryBody) {
    const bodyProperties = Object.getOwnPropertySymbols(queryBody)
    let statusBody = {}

    if (bodyProperties.length) {
      if (bodyProperties[0] === operatorAliases.$or) {
        statusBody[operatorAliases.$and] = [queryBody]
      } else {
        statusBody = queryBody
      }

      if (status === 'returned') {
        statusBody[operatorAliases.$and].push({
          status: 'returned'
        })
      } else if (status === 'rejected') {
        statusBody[operatorAliases.$and].push({
          status: 'rejected'
        })
      } else if (status === 'ontime') {
        statusBody[operatorAliases.$and].push({
          status: 'returned'
        })
        statusBody[operatorAliases.$and].push({
          date_due: {
            [operatorAliases.$gte]: Sequelize.literal('date_returned')
          }
        })
      } else if (status === 'overdue') {
        statusBody[operatorAliases.$and].push({
          status: 'returned'
        })
        statusBody[operatorAliases.$and].push({
          date_due: {
            [operatorAliases.$lt]: Sequelize.literal('date_returned')
          }
        })
      }
    }

    return statusBody
  }

  /**
   * Appends filter that would fetch resources with a given availability.
   *
   * @method requestResourceAvailability
   * @param {String} availability availability determinant
   * @param {Object} queryBody query body to be injected with filter
   * @return {Object} returns statusBody
   */
  requestResourceAvailability(availability, queryBody) {
    const isAliased = Object.getOwnPropertySymbols(queryBody)
    let availabilityBody = {}

    if (isAliased.length) {
      if (availability === 'available') {
        isAliased.forEach((operatorAlias) => {
          availabilityBody = queryBody
          availabilityBody[operatorAlias].push({
              '$resource.count$': {
                [operatorAliases.$gt]: Sequelize.literal('resource.borrowed')
              }
          })
        })
      } else if (availability === 'notAvailable') {
        isAliased.forEach((operatorAlias) => {
          availabilityBody = queryBody
          availabilityBody[operatorAlias].push({
              '$resource.count$': {
                [operatorAliases.$lte]: Sequelize.literal('resource.borrowed')
              }
          })
        })
      } else {
        availabilityBody = queryBody
      }

      return availabilityBody
    }

    if (availability === 'available') {
      availabilityBody[operatorAliases.$and] = [
        queryBody,
        {
          '$resource.count$': {
            [operatorAliases.$gt]: Sequelize.literal('resource.borrowed')
          }
        }
      ]
    } else if (availability === 'notAvailable') {
      availabilityBody[operatorAliases.$and] = [
        queryBody,
        {
          '$resource.count$': {
            [operatorAliases.$lte]: Sequelize.literal('resource.borrowed')
          }
        }
      ]
    } else {
      availabilityBody = queryBody
    }

    return availabilityBody
  }

  /**
   * Excludes resources with count value that is less than 1.
   *
   * @method buildQueryExcludeNoCount
   * @param {Object} queryBody query body to be injected with filters
   * @return {Object} returns filteredBody
   */
  buildQueryExcludeNoCount(queryBody) {
    let filteredBody = {}
    const isAliased = Object.getOwnPropertySymbols(queryBody)
    const exclusionQuery = {
      [operatorAliases.$not]: {
        [operatorAliases.$and]: {
          count: {
            [operatorAliases.$lte]: 0
          },
          classification: 'document'
        }
      }
    }

    if (isAliased.length) {
      isAliased.forEach((operatorAlias) => {
        filteredBody = queryBody
        filteredBody[operatorAlias]
          .push({ [operatorAliases.$and]: exclusionQuery })
      })
    } else {
      filteredBody[operatorAliases.$and] = [
        queryBody,
        { [operatorAliases.$and]: exclusionQuery }
      ]
    }

    return filteredBody
  }

  /**
   * Excludes resources with count value that is greater than 0.
   *
   * @method buildQueryExcludeWithCount
   * @param {Object} queryBody query body to be injected with filters
   * @return {Object} returns filteredBody
   */
  buildQueryExcludeWithCount(queryBody) {
    let filteredBody = {}
    const isAliased = Object.getOwnPropertySymbols(queryBody)
    const exclusionQuery = {
      [operatorAliases.$not]: {
        [operatorAliases.$or]: {
          count: {
            [operatorAliases.$gt]: 0
          },
          classification: 'digital'
        }
      }
    }

    if (isAliased.length) {
      isAliased.forEach((operatorAlias) => {
        filteredBody = queryBody
        filteredBody[operatorAlias]
          .push({ [operatorAliases.$and]: exclusionQuery })
      })
    } else {
      filteredBody[operatorAliases.$and] = [
        queryBody,
        { [operatorAliases.$and]: exclusionQuery }
      ]
    }

    return filteredBody
  }
}
