import jsYaml from 'js-yaml'
import fs from 'fs'

/**
 * Class for reading of configuration files.
 *
 * @class ymlLoader
 */
export default class ymlLoader {

  /**
   * Reads custom configuration files.
   *
   * @method loadConfig
   * @param {string} pathToConfig path and filename to configuration file
   * @return {Object} returns configuration object
   */
  loadConfig(pathToConfig) {
    return jsYaml.safeLoad(fs.readFileSync(`${pathToConfig}.yml`, 'utf8'))
  }

  /**
   * Reads global configuration files.
   *
   * @method loadGlobalConfig
   * @param {string} filename filename of configuration file
   * @return {Object} returns configuration object
   */
  loadGlobalConfig(filename) {
    return jsYaml.safeLoad(fs.readFileSync(`config/${filename}.yml`, 'utf8'))
  }
}
