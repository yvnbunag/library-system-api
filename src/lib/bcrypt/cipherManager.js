/* eslint dot-notation: off */
import Cryptorjs from 'cryptorjs'

/**
 * Simple encryption/decryption class.
 *
 * @class cipherManager
 * @constructor
 */
export default class cipherManager {
  constructor() {
    this.cryptorjs = new Cryptorjs(this.getEncryptionKey())
    this.hashingKey = this.getObjectHashingKey()
  }

  /**
   * Encrypts string parameter.
   *
   * @method encrypt
   * @param {String} string value to be encrypted
   * @return {Function} returns Cryptorjs.encode as callback function
   */
  encrypt(string) {
    return this.cryptorjs.encode({
      value: String(string),
      hashingKey: this.hashingKey
    })
  }

  /**
   * Decrypts string parameter.
   *
   * @method decrypt
   * @param {String} encryptedString value to be decrypted
   * @return {Function} returns Cryptorjs.decode as callback function
   */
  decrypt(encryptedString) {
    return this.cryptorjs.decode(encryptedString).value
  }

  /**
   * Reads encryption key from parameter config file.
   *
   * @method getEncryptionKey
   * @return {String} returns encryption key
   */
  getEncryptionKey() {
    return process.env.ENCRYPTION_KEY
  }

  /**
   * Reads object hashing key from parameter config file.
   *
   * @method getObjectHashingKey
   * @return {String} returns object hashing key
   */
  getObjectHashingKey() {
    return process.env.HASHING_KEY
  }
}
