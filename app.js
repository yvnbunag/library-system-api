/* eslint-disable */
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser')
const createError = require('http-errors')
const express = require('express')
const logger = require('morgan')
const path = require('path')
const bearerToken = require('express-bearer-token')
const busboy = require('connect-busboy')
const cors = require('cors')

/* Global namespace aliases initialization. */
global.srcBase
global.__basedir = __dirname

/* Source file directory pointer initialization dependent on environment. */
if (process.env.NODE_ENV ==='production') {
  srcBase = '@src'
}
else {
  require('source-map-support').install()
  require("babel-register")
  srcBase = '~src'
}


require('module-alias/register')      /* Package alias initialization. */
require(`${srcBase}/srcModuleAlias`)  /* Custom module alias initialization. */

/* Routers instantiation. */
const indexRouter = require(`${srcBase}/routes/index`).default
const resourcesBorrowRouter = require(`${srcBase}/routes/resources.borrow`).default
const resourcesRouter = require(`${srcBase}/routes/resources`).default
const securityRouter = require(`${srcBase}/routes/security`).default
const usersRouter = require(`${srcBase}/routes/users`).default
const workersRouter = require(`${srcBase}/routes/workers`).default

/* Global runtime functions instantiation. */
const ErrorHandler = require(`${srcBase}/lib/errorHandler`).default
const Worker = require(`${srcBase}/workers/workerRunner`).default
global.errorHandler = new ErrorHandler
global.worker = new Worker

const app = express()

// View engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'pug')

app.use(logger('dev'))

/* Middleware parsers initialization */
app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
// app.use(express.json())
// app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(bearerToken())
app.use(busboy())

/* Router middlewares */
app.use('/', indexRouter)
app.use('/resources/borrow', resourcesBorrowRouter)
app.use('/resources', resourcesRouter)
app.use('/security', securityRouter)
app.use('/users', usersRouter)
app.use('/workers', workersRouter)

/* Error handling middleware */
app.use(errorHandler.handle)

/* Nonexistent route handler */
app.use(errorHandler.notFound)

// Catch 404 and forward to error handler
// app.use(function(req, res, next) {
//   next(createError(404))
// });

// // Custom error handler
// app.use(function(err, req, res, next) {
//   // Set locals, only providing error in development
//   res.locals.message = err.message
//   res.locals.error = req.app.get('env') === 'development' ? err : {}

//   // Render the error page
//   res.status(err.status || 500)
//   res.render('error')
// });

module.exports = app
