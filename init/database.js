/* eslint-disable */
import { readdir, readFile } from 'fs/promises'

require('make-runnable/custom')({ printOutputFrame: false })

import 'module-alias/register'
import env from 'node-env-file'

import '~src/srcModuleAlias'
import BaseModel from '@models/baseModel'

/**
 * Terminal runnable function to sync database
 *
 * @param {Object} [options] Sync options
 * @param {String} [options.force] Flag to force database sync (existing
 *  table structures would be overridden)
 * @param {String} [options.modelsDirectory] Directory of models to sync
 * @param {String} [options.environment] Environment variables file
 */
export async function sync(options = {}) {
  const {
    force = null,
    modelsDirectory = 'src/domain/models',
    environment = '.env'
  } = options

  env(environment)

  let stage = 0

  try {
    console.log('Syncing models...')

    const files = await readdir(modelsDirectory, { withFileTypes: true })
    const syncFiles = files.filter(
      (dirent) => !dirent.isDirectory() && dirent.name !== 'baseModel.js'
    )

    const { length } = syncFiles

    for (let ctr = 0; ctr < length; ctr++) {
      stage++
      const Model = require(`@models/${syncFiles[ctr].name}`).default
      const model = new Model()
      const name = syncFiles[ctr].name.replace('Model.js', '')
      const upperCasedName = `${name.charAt(0).toUpperCase()}${name.slice(1)}`

      console.log(`[${stage}/${length}] Syncing ${name} table`)
      await model.sync({ force })
      console.log(`[${stage}/${length}] ${upperCasedName} table synced`)
    }

  } catch (err) {
    console.log(`Sync failed at stage ${stage}\nReason: ${err.message}`)
    return process.exit(1)
  }

  console.log('Models sync finished')

  process.exit(0)
}

/**
 * Terminal runnable function to seed database
 *
 * @param {Object} [options] Sync options
 * @param {String} [options.dumpFile] SQL file to seed database
 * @param {String} [options.environment] Environment variables file
 */
export async function seed(options = {}) {
  const {
    dumpFile = 'vars/library_system_2-5-19.sql',
    environment = '.env'
  } = options

  env(environment)

  console.log(`Importing dump file from ${dumpFile}`)

  const { sequelize } = new BaseModel()
  const rawQueries = await readFile(dumpFile, { encoding: 'utf8' })
  const queries = rawQueries.split(";\n")

  for (const query of queries) {
    if (query.length !== 0 && !query.match(/\/\*/)) {
      await sequelize.query(query, { raw: true })
    }
  }

  console.log('Dump file import finished')

  process.exit()
}
